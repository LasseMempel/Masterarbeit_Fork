# Hellenistische Schauergeschichte? Sentiment Analyse antiker Werke mit Python und Transformern. 
*work in progress*


# Fragestellung
Die Arbeit untersucht erstmalig antike Werke quantitativ auf ihre "Tragik". Tragik wird dabei nach Polybios und Aristoteles an den Merkmalen Negativität, Emotionalität und Leid festgemacht.

# Umsetzung
Die antiken Werke werden aus XML- und HTML-Dateien eingelesen, bereinigt und nach Abschnitten gegliedert als JSON-Dateien gespeichert. Anschließend klassifizieren Transformer-Modelle über Huggingface Pipelines alle Sätze der Werke nach ihrer Polarität (positiv/negativ) bzw. Emotion (Angry, Disgusted, Fearful, Happy, Sad, Surprised, Neutral). Möglichst genaue Modelle wurden zuvor durch Validierung der Klassifizierung von Test-Datensets ermittelt. Emotionalität und Leid werden als Anteil aller Sätze, die nicht neutral, bzw. mit einer positiven Emotion klassifiziert werden, definiert.
Die Werte für "Tragik" ergeben sich nach der Formel: 0.5 * -Negativität + 0.25 * Emotionalität + 0.25 * Leid.
Die Ergebnisse für die Werke und ihre Abschnitte werden in Tabellen gespeichert und durch passende Visualisierungen veranschaulicht.

# Ergebnisse
Die berechneten Werte für Tragik zeigen sich beim Vergleich mit den Beurteiligungen klassischer qualitativer Forschung als plausibel. So weist z.B. Flavius Josephus "War of the Jews" die höchste "Tragik" auf, Polybios und Arrians, die als nüchtern gelten, dagegen niedrigere. Sachliche Genres wie Philosophie und Geographie zeigen ebenfalls niedrige Werte. Während die Polarität für unterschiedliche Werke der Epochen variiert, lässt sich für Emotionalität und Leid eine Zunahme von der Klassik über den Hellenismus bis zur Kaiserzeit feststellen.   

# Einschränkungen
Die Sprachmodelle konnten nicht für alle untersuchten Genres mit passenden Datensets validiert werden. Bei der Bestimmung der Polarität von Tragödien-Sätzen zeigten sie sich sehr performant, bei Ilias-Versen deutlich weniger. Aus den drei besten Modellen wurde ein Ensemble für das Gesamtergebnis gebildet, da diese in ihrer Tendenz zum Positiven und Negativen voneinander abwichen. 
Die Emotionsklassifizierung konnte nur an einem domänenfremden Datenset von Märchen-Sätzen validiert werden. Hierbei zeigten sich bei insgesamt guter Genauigkeit große Unterschiede zwischen den Klassen. 

# Übersicht der Codebasis nach Ordnern und Dateien

**/Data-Scraping: Einlesen der Werksdateien und Validierungsdateien**
- xmlRead: Hauptskript zum Einlesen. Erzeugt in /sourceDict eine JSON-Datei für jedes Werk
- Arrian_readHTML und Dionysios_readHTML: Module zum Webscrapen
- xmlCheck: Skript zum Ermitteln der Tags in den XML-Dateien
- normalizeText: Modul zum Bereinigen von Strings
- Skripte zum Einlesen der Validierungstexte:
  - aeschylosToDict
  - affectTalesToDict
  - iliadesToDict
- /Aeschylos: Sätze Aeschylos Datenset
- /Iliaden: Sätze Ilias Datenset (Gelöscht wegen Urheberrecht)
- /literaryEmotions/affect tales: Sätze fairytales Dataset 
- /PerseusXML: Perseus-Werkstexte


**/Sentiment-Analysis: Sentiment-Analyse**
- calcSentFromDict: Bestimmen und Ausführen verschiedener SA-Pipelines für alle Werke. Erzeugt in /sentDict eine JSON-Datei für jedes Werk und Modell

**/Analyse: Validierung der Modelle und Analyse der Ergebnisse**
- /Modeldata: Skripte zur Validierung und Berechnung von IAA
  - analyseModelsAeschylos: Validierung Aeschylos
  - analyseModelsFairyTales: Validierung fairytales
  - analyseModelsIlias: Validierung Ilias
  - InterAnnotatorAgreementAeschylos
  - InterAnnotatorAgreementIlias
  - /sourceDict: Erzeugte Validierungs JSONS
  - /sentDict: Sentiment-Werte für Validierungstexte
  - /Modelperformance: Tabellen mit Validierungsergebnissen
- analyseSents: Berechnen, Zusammenführen und Abspeichern der Ergebnisse der Untersuchungswerke 
- /sentData: Ergebnisse in Tabellenform

**/Visualisierungen: Erzeugung von Abbildungen aus den generierten Daten**
- plotSentiments: Erstellung unterschiedlicher Plots










