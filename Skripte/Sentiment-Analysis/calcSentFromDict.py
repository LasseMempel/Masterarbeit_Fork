import json
import os
import nltk
from transformers import pipeline
from optimum.bettertransformer import BetterTransformer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from optimum.pipelines import pipeline as optimumPipeline
from nltk.tokenize import sent_tokenize

nltk.download("punkt")
nltk.download("vader_lexicon")

def zeroShotPolarityPipeline(text):
    sentiments = polarityPipeline(text, zeroShotPolarityTags, multi_label=False)
    sentiments = [[{"label" : key, "score" : value} for key, value in zip(sentiment["labels"], sentiment["scores"])] for sentiment in sentiments]
    return sentiments 

def zeroShotEmotionPipeline(text):
    sentiments = emotionPipeline(text, zeroShotEmotionTags, multi_label=True) # multi_label=False
    sentiments = [[{"label" : key, "score" : value} for key, value in zip(sentiment["labels"], sentiment["scores"])] for sentiment in sentiments]
    return sentiments

def vaderPipeline(text):
    sentiments = [dictionaryPipeline(x) for x in text]
    sentiments = [[{"label" : key, "score" : value} for key, value in calculation.items()] for calculation in sentiments]
    return sentiments

def calculateSentencePolarity(dictionaryList):
    # Vader hat vier dictionary Einträge, die anderen Modelle haben drei
    if len(dictionaryList) == 4 and dictionaryList[3]["score"] == "compound":
        # Vader beinhaltet einen compound score, der aus den anderen scores, Negationen und intensifiern berechnet wird
        compoundScore = dictionaryList[3]["score"] 
        if compoundScore > 1/3:
            sentiment = 1
        elif compoundScore < -1/3:
            sentiment = -1
        else:
            sentiment = 0
        return(sentiment, compoundScore) # We additionally save the compound score for emotion intensity
    else:
        # 2 Klassen Modelle erhalten einen neutralen Score von 0
        neutralSentiment = 0
        for Class in dictionaryList:
            classLabel = Class["label"].lower()
            if classLabel in ["positive", "label_1", "pos"]:
                positiveSentiment = Class["score"] 
            elif classLabel in ["negative", "label_0", "neg"]:
                negativeSentiment = Class["score"]
            elif classLabel in ["neutral", "label_2", "neu"]:
                neutralSentiment += Class["score"]
            # "joheras/clasificador-poem-sentiment" hat einen zusätzlichen mixed score, der hier mit neutral verrechnet wird 
            elif classLabel == "label_3":
                neutralSentiment += Class["score"]
        if positiveSentiment > max(negativeSentiment, neutralSentiment):
                sentiment = (1, positiveSentiment) # sentDic values are (positive, negative, neutral)
        elif negativeSentiment > max(positiveSentiment, neutralSentiment):
            sentiment = (-1, negativeSentiment)
        elif neutralSentiment > max(positiveSentiment, negativeSentiment):
            sentiment = (0, neutralSentiment)
        else: #Für den Fall, dass zwei Klassen gleich groß sind, ist das Ergebnis neutral
            sentiment = (0, neutralSentiment)
        return sentiment

def calculateSectionSentiment(text, sentimentPipeline, key):
    sectionSentences = sent_tokenize(text)
    if sectionSentences == []:
        print(f"Error: No sentences in section {key}!")
        #raise Exception("Error: No sentences in section!")
        return [("", None)]
    else:
        sentiments = sentimentPipeline(sectionSentences)
        # emotion classification models and zeroshot models used for emotion classification use more than 4 labels
        if len(sentiments[0]) > 4:
            return [(sentence, [(Class["label"], Class["score"]) for Class in sentiment]) for sentence, sentiment in zip(sectionSentences, sentiments)]
        # polarity classification models, zero shot models used for polarity classification and vader use less than 5 labels
        else:
            return [(sentence, calculateSentencePolarity(sentiment)) for sentence, sentiment in zip(sectionSentences, sentiments)]

def getSentFromSourceDict(sourceDict, sentDict, sentimentPipeline):
    for key in sourceDict:
        if type(sourceDict[key]) == str:
            sentDict[key] = calculateSectionSentiment(sourceDict[key], sentimentPipeline, key)
        elif type(sourceDict[key]) == dict:
            sentDict[key] = getSentFromSourceDict(sourceDict[key], sourceDict[key], sentimentPipeline)
        else:
            raise Exception("Error: Unexpected type of value in sourceDict!")               
    return sentDict

# Reduzierung fragmentarischer Werke auf vollständige Bücher
def historyPruning(file, sourceDict):
    if "Appian_Roman History" in file:
        fragmentedBooks = ["BookFragments", "Kings"," Italy", "Samnite History", "Gallic History", "Sicily and the Other Islands", \
            "Punic Wars", "Numidian Affairs", "Macedonian Affairs", "Illyrian Wars", "Syrian Wars"]
        for key in fragmentedBooks:
            sourceDict.pop(key)
    if "Diodorus Siculus_Library" in file:
        fragmentedBooks = ["Book9","Book10","Book10a"]
        for key in fragmentedBooks:
            sourceDict.pop(key)
    if "Dionysios of Halikarnassus_Roman Antiquities" in file:
        sourceDict = dict(list(sourceDict.items())[0:11])
    if "Polybius_Histories" in file:
        sourceDict = dict(list(sourceDict.items())[0:5])
    return sourceDict

def generateSentDicts(rootDir, ModelName, modelDescriptor, sentimentPipeline):
    for subdir, dirs, files in os.walk(rootDir):
        for file in files:
            fileName = os.path.join(subdir, file)
            with open(fileName, "r") as f:
                sourceDict = json.load(f)
            fileData = fileName.split(".json")[0].split("/")[-1]
            author, title, genre = fileData.split("_")[0], fileData.split("_")[1], subdir.split("/")[-1]
            print(f"calculating sentiment for: {author} {title}")
            projectedFolder = f"{dataFolder}/Skripte/sentDict/{genre}"
            if not os.path.exists(projectedFolder):
                os.makedirs(projectedFolder)
            if not os.path.isfile(f"{projectedFolder}/{author}_{title}_{ModelName}{modelDescriptor}_sentDict.json") or os.path.getsize(f"{projectedFolder}/{author}_{title}_{ModelName}{modelDescriptor}_sentDict.json") == 0: 
                with open(f"{projectedFolder}/{author}_{title}_{ModelName}{modelDescriptor}_sentDict.json","w") as f: 
                    sourceDict = historyPruning(file, sourceDict)
                    sentDict = getSentFromSourceDict(sourceDict, {}, sentimentPipeline)
                    json.dump(sentDict, f)
                    
            else:
                print("SentDict already exists!")

def modelToSentDicts(rootDir, modelDescriptor, Models, modelDict):
    for Model in Models:
        print(Model)
        ModelName = Model.replace("/","-")
        if Model in modelDict["dictionaryModels"]:
            global dictionaryPipeline
            dictionaryPipeline = SentimentIntensityAnalyzer().polarity_scores
            sentimentPipeline = vaderPipeline
            generateSentDicts(rootDir, ModelName, "", sentimentPipeline)
        elif Model in modelDict["zeroShotModels"]:
            global polarityPipeline
            try:
                polarityPipeline = optimumPipeline("zero-shot-classification", model=Model, top_k=None , truncation=True, padding=True, framework="pt", device=0, accelerator="bettertransformer")
                
            except:
                print("optimum pipeline not functional")
                polarityPipeline = pipeline("zero-shot-classification", model=Model, top_k=None , truncation=True, padding=True, framework="pt", device=0, accelerator="bettertransformer")
            sentimentPipeline = zeroShotPolarityPipeline
            generateSentDicts(rootDir, ModelName, modelDescriptor, sentimentPipeline)
        elif Model in modelDict["transformerModels"] or Model in modelDict["emotionModels"]:
            sentimentPipeline = pipeline("sentiment-analysis", model=Model, top_k=None , truncation=True, padding=True, framework="pt", device=0)
            generateSentDicts(rootDir, ModelName, "", sentimentPipeline)
        if Model in modelDict["zeroShotEmotionModels"]:
            global emotionPipeline
            try:
                emotionPipeline = optimumPipeline("zero-shot-classification", model=Model, top_k=None , truncation=True, padding=True, framework="pt", device=0, accelerator="bettertransformer")
            except:
                print("optimum pipeline not functional")
                emotionPipeline = pipeline("zero-shot-classification", model=Model, top_k=None , truncation=True, padding=True, framework="pt", device=0,  accelerator="bettertransformer")
            sentimentPipeline = zeroShotEmotionPipeline
            generateSentDicts(rootDir, ModelName, modelDescriptor, sentimentPipeline)

dataFolder = "/home/rudolph/repos/masterarbeit"
rootDir = f"{dataFolder}/Skripte/Analyse/Modeldata/sourceDict/Aeschylos/"

modelDict = {"transformerModels" : ["distilbert-base-uncased-finetuned-sst-2-english", "siebert/sentiment-roberta-large-english", "j-hartmann/sentiment-roberta-large-english-3-classes", "joheras/clasificador-poem-sentiment"],
             "dictionaryModels" : ["VADER"],
             "emotionModels" : [], 
             "zeroShotModels" : ["facebook/bart-large-mnli", "valhalla/distilbart-mnli-12-9", "MoritzLaurer/deberta-v3-large-zeroshot-v1", "roberta-large-mnli"],
             "zeroShotEmotionModels" : []
            }

Models = modelDict["transformerModels"] + modelDict["zeroShotModels"] + modelDict["dictionaryModels"]

modelDescriptor = "3Classes"
global zeroShotPolarityTags 
zeroShotPolarityTags = ["positive", "negative", "neutral"]
global zeroShotEmotionTags 
zeroShotEmotionTags =["angry", "disgusted", "fearful", "happy", "sad", "surprised", "neutral"]

modelToSentDicts(rootDir, modelDescriptor, Models, modelDict)