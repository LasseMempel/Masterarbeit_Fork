import json
import numpy as np
import matplotlib.pyplot as plt
import glob
import pandas as pd
import csv
from scipy.stats import pearsonr
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, classification_report

dataFolder = "/home/rudolph/repos/masterarbeit"
goldenCsv = dataFolder + "/Skripte/Analyse/Modeldata/QualtricsSurvey.csv"
with open(goldenCsv, "r") as file:
    data = list(csv.reader(file,delimiter=","))
goldenList = []
for i in range(1, len(data)):
    responsesSentence = 8
    responsesContext = 10
    sentenceIndex = responsesContext
    lineValues = (data[i][sentenceIndex]).replace("[","").replace("]","")
    lineValues = [int(x) for x in lineValues.split(",")]
    neutralLenght = len([x for x in lineValues if x == 3])
    positiveLenght = len([x for x in lineValues if x in [1,2]])
    negativeLenght = len([x for x in lineValues if x in [4,5]])
    if neutralLenght > max(positiveLenght, negativeLenght):
        goldenList.append(0)
    elif positiveLenght > max(neutralLenght, negativeLenght):
        goldenList.append(1)
    elif negativeLenght > max(neutralLenght, positiveLenght):
        goldenList.append(-1)
    # Für den Fall, dass zwei Klassen gleich groß sind, wird die Zeile nicht berücksichtigt
    # Dies betrifft 3 Beispiele
    else: 
        goldenList.append(None)
goldenListValids = [x for x in goldenList if x != None]
goldMean, goldMedian, goldStd = np.mean(goldenListValids), np.median(goldenListValids), np.std(goldenListValids)
goldPos = len([x for x in goldenList if x == 1])
goldNeg = len([x for x in goldenList if x == -1])
goldNeu = len([x for x in goldenList if x == 0]) 
goldTotal = goldPos + goldNeg + goldNeu
goldDist= str(int(goldPos/goldTotal*100))+"/"+str(int(goldNeg/goldTotal*100))+"/"+str(int(goldNeu/goldTotal*100))

def generateSentList(fileName):
    with open(fileName) as f:
        data = json.load(f)
        data = data["Book1"]
        sentList = []
        for line in data:
            if data[line][0][1] != None:
                lineValues = data[line][0][1][0]
            else:
                lineValues = None
            sentList.append(lineValues)
    return sentList

def calculateEmotionMetrics(metricsDict, emotion, annotationList):
    emotionInstances = len([x for x in annotationList if x == int(emotion)])
    emotionPrecision = metricsDict[emotion]["precision"]
    emotionRecall = metricsDict[emotion]["recall"]
    emotionF1 = metricsDict[emotion]["f1-score"]
    return emotionInstances, emotionPrecision, emotionRecall, emotionF1

df = pd.DataFrame(columns=["model", "translation", "accuracy", "precision", "recall", "f1", "%POS/NEG/NEU"]) 
ClassesDf = pd.DataFrame(columns=["model", "translation", "sentiment", "instances", "precision", "recall", "f1"]) 

emotionDict = {"-1": "negative", "0": "neutral", "1": "positive"}

sentFiles = sorted(glob.glob(dataFolder + "/Skripte/Analyse/Modeldata/sentDict/Aeschylos/*.json"))
sentFiles = [x for x in sentFiles if "emotion" not in x]
for file in sentFiles:
    model = file.split("Aeschylos_")[1].split("_sentDict.json")[0] 
    model = model.split("-")[0] if model != "j-hartmann-sentiment-roberta-large-english-3-classes" else "j-hartmann"
    translation = file.split("_")[0].split("/")[-1]
    sentList = generateSentList(file)
    sentGolden = [x for x in goldenList]
    for i in range(len(sentList)):
        if None in [sentGolden[i], sentList[i]]:
            sentList[i], sentGolden[i] = None, None
    sentGolden = [x for x in sentGolden if x != None]
    sentList = [x for x in sentList if x != None]
    accuracy = accuracy_score(sentGolden, sentList)
    precision = precision_score(sentGolden, sentList, average="weighted", zero_division=0)
    recall = recall_score(sentGolden, sentList, average="weighted", zero_division=0)
    f1 = f1_score(sentGolden, sentList, average="weighted", zero_division=0)
    mean, median, std = np.mean(sentList), np.median(sentList), np.std(sentList)
    positives = len([x for x in sentList if x == 1])
    negatives = len([x for x in sentList if x == -1])
    neutrals = len([x for x in sentList if x == 0])
    totals = positives + negatives + neutrals
    cldist = str(int(positives/totals*100))+"/"+str(int(negatives/totals*100))+"/"+str(int(neutrals/totals*100)) 
    df.loc[len(df)] = [model, translation, accuracy, precision, recall, f1, cldist]
    metricsDict = classification_report(sentGolden, sentList, output_dict=True, zero_division=0)
    for emotion in ["-1", "0", "1"]:
        emotionInstances, emotionPrecision, emotionRecall, emotionF1 = calculateEmotionMetrics(metricsDict, emotion, sentGolden)
        ClassesDf.loc[len(ClassesDf)] = [model, translation, emotionDict[emotion], emotionInstances, emotionPrecision, emotionRecall, emotionF1]

zeroShotModels = ["facebook", "roberta", "valhalla", "MoritzLaurer"]

df = df.sort_values(by="f1", ascending=False)
df = df.drop(df.columns[[-1]], axis=1)
# drop instances
#df = df.drop(df.columns[[-1]], axis=1)
pd.set_option("display.max_rows", None, "display.max_columns", None)

zeroDf = df[df["model"].isin(zeroShotModels)]
print(zeroDf[["model", "translation", "accuracy", "precision", "recall", "f1"]].to_string(index=False))
zeroDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/aeschylosZero.csv", sep=",", index=False, encoding="utf-8")
print("\n")

noZeroDf = df[~df["model"].isin(zeroShotModels)]
print(noZeroDf[["model", "translation", "accuracy", "precision", "recall", "f1"]].to_string(index=False))
noZeroDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/aeschylosNoZero.csv", sep=",", index=False, encoding="utf-8")
print("\n")

ClassesDf = ClassesDf.sort_values(by="model", ascending=False)
ClassesDf = ClassesDf.round(2)
pd.set_option("display.max_rows", None, "display.max_columns", None)
ClassesDf = ClassesDf.drop(ClassesDf.columns[[3]], axis=1)

ClassesZeroDf = ClassesDf[ClassesDf["model"].isin(zeroShotModels)]
print(ClassesZeroDf[["model", "translation", "sentiment", "precision", "recall", "f1"]].to_string(index=False))
ClassesZeroDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/aeschylosClassesZero.csv", sep=",", index=False, encoding="utf-8")
print("\n")

ClassesNoZeroDf = ClassesDf[~ClassesDf["model"].isin(zeroShotModels)]
print(ClassesNoZeroDf[["model", "translation", "sentiment", "precision", "recall", "f1"]].to_string(index=False))
ClassesNoZeroDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/aeschylosClassesNoZero.csv", sep=",", index=False, encoding="utf-8")



