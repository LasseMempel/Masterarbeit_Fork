Question-Number: A question (sentence) asked students for labeling.
Dialogue-Size: The number of sentences in a dialogue.
Essay-Name: Play name
Full-Dialogue: The whole dialogue in the play from which the current sentence is extracted.
Dialogue: The part of dialogue from the whole dialogue where it includes context for the current sentence.
Sentence: A sentence (question) asked students to label the sentiment.
Sentence-Length: Length of the sentence.
S_Responses: Sentiment responses for the sentence from each of the students.
S_No of responses: The number of responses for the sentence. Also, the number of students who responded to the question (sentence).
D_Responses: Sentiment responses for the dialouge from each of the students.
D_No of responses: The number of responses for the dialouge. Also, the number of students who responded to the dialouge.
WordChoicesByQualtricsSurvey: Word choices from the students.


Sentiment Response:

1: Extremely positive
2:Moderately positive
3:Neutral
4:Moderately negative
5:Extremely negative



