import json
import numpy as np
import matplotlib.pyplot as plt
import glob
import pandas as pd
from scipy.stats import pearsonr
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, classification_report

dataFolder = "/home/rudolph/repos/masterarbeit"
goldenDir = dataFolder + "/Skripte/Analyse/Modeldata/Märchen Annotationen/"
rootDir = dataFolder + "/Skripte/Analyse/Modeldata/sentDict/Märchen/"

emotionDict =  {"angry": "angry",
                "anger": "angry",
                "disgusted": "disgusted", 
                "disgust": "disgusted",
                "fearful": "fearful",
                "fear": "fearful",
                "happy": "happy", 
                "joy": "happy",
                "neutral": "neutral", 
                "sad": "sad",
                "sadness": "sad",
                "surprised": "surprised",
                "surprise": "surprised"
                }

emotionList = ["angry", "disgusted", "fearful", "happy", "neutral", "sad", "surprised"]
modelDf = pd.DataFrame(columns=["model", "author", "accuracy", "precision", "recall", "f1", "instances"])
emotionDf = pd.DataFrame(columns=["emotion", "author", "model", "precision", "recall", "f1", "instances"])
classDistributionsDf = pd.DataFrame(columns=["model", "author", "angry", "disgusted", "fearful", "happy", "neutral", "sad", "surprised", "instances"])
weightedEmotionDf = pd.DataFrame(columns=["emotion", "model", "precision", "recall", "f1", "instances"])
weightedModelsDf = pd.DataFrame(columns=["model", "accuracy", "precision", "recall", "f1", "instances"])
weightedClassDistributionsDf = pd.DataFrame(columns=["model", "angry", "disgusted", "fearful", "happy", "neutral", "sad", "surprised", "instances"])

annotationDict = {}
modelDict = {}
for file in glob.glob(goldenDir + "*.json"):
    with open(file, "r") as f:
        workDict = json.load(f)
        author = file.split("_AffectTales")[0].split("/")[-1]
        annotationList = [workDict[x] for x in workDict]
        annotationDict[author] = annotationList

for file in glob.glob(rootDir + "*.json"):
    with open(file, "r") as f:
        workDict = json.load(f)
        model = file.split("_")[2]
        author = file.split("_AffectTales")[0].split("/")[-1]
        modelList = [emotionDict[workDict[x][0][1][0][0]] for x in workDict]
        if not model in modelDict:
            modelDict[model] = {}
        modelDict[model][author] = modelList

def calculateMetrics(annotationList, modelList):
    modelInstances = len(annotationList)
    modelAccuracy = accuracy_score(annotationList, modelList)
    modelPrecision = precision_score(annotationList, modelList, average="weighted", zero_division=0)
    modelRecall = recall_score(annotationList, modelList, average="weighted", zero_division=0)
    modelF1 = f1_score(annotationList, modelList, average="weighted", zero_division=0)
    return modelInstances, modelAccuracy, modelPrecision, modelRecall, modelF1 

def calculateEmotionMetrics(metricsDict, emotion, annotationList):
    emotionInstances = len([x for x in annotationList if x == emotion])
    emotionPrecision = metricsDict[emotion]["precision"]
    emotionRecall = metricsDict[emotion]["recall"]
    emotionF1 = metricsDict[emotion]["f1-score"]
    return emotionInstances, emotionPrecision, emotionRecall, emotionF1

for model in modelDict:
    for author in modelDict[model]:
        modelInstances, modelAccuracy, modelPrecision, modelRecall, modelF1 = calculateMetrics(annotationDict[author], modelDict[model][author])
        modelDf.loc[len(modelDf)] = [model, author, modelAccuracy, modelPrecision, modelRecall, modelF1, modelInstances]
        modelList = modelDict[model][author]
        classDistributionsDf.loc[len(classDistributionsDf)] = [model, author, len([x for x in modelList if x == "angry"])/len(modelList)*100, len([x for x in modelList if x == "disgusted"])/len(modelList)*100, len([x for x in modelList if x == "fearful"])/len(modelList)*100, len([x for x in modelList if x == "happy"])/len(modelList)*100, len([x for x in modelList if x == "neutral"])/len(modelList)*100, len([x for x in modelList if x == "sad"])/len(modelList)*100, len([x for x in modelList if x == "surprised"])/len(modelList)*100, len(modelList)]
        metricsDict = classification_report(annotationDict[author], modelDict[model][author], output_dict=True, zero_division=0)
        for emotion in emotionList:
            emotionInstances, emotionPrecision, emotionRecall, emotionF1 = calculateEmotionMetrics(metricsDict, emotion, annotationDict[author])
            emotionDf.loc[len(emotionDf)] = [emotion, author, model, emotionPrecision, emotionRecall, emotionF1, emotionInstances]
    fullAnnotationList = []
    fullModelList = []
    for author in modelDict[model]:
        fullAnnotationList.extend(annotationDict[author])
        fullModelList.extend(modelDict[model][author])
    modelInstances, modelAccuracy, modelPrecision, modelRecall, modelF1 = calculateMetrics(fullAnnotationList, fullModelList)
    weightedModelsDf.loc[len(weightedModelsDf)] = [model, modelAccuracy, modelPrecision, modelRecall, modelF1, modelInstances]
    metricsDict = classification_report(fullAnnotationList, fullModelList, output_dict=True, zero_division=0)
    for emotion in emotionList:
        emotionInstances, emotionPrecision, emotionRecall, emotionF1 = calculateEmotionMetrics(metricsDict, emotion, fullAnnotationList)
        weightedEmotionDf.loc[len(weightedEmotionDf)] = [emotion, model, emotionPrecision, emotionRecall, emotionF1, emotionInstances]
    
for author in annotationDict:
    annotationList = annotationDict[author]
    classDistributionsDf.loc[len(classDistributionsDf)] = ["goldstandard", author, len([x for x in annotationList if x == "angry"])/len(annotationList)*100, len([x for x in annotationList if x == "disgusted"])/len(annotationList)*100, len([x for x in annotationList if x == "fearful"])/len(annotationList)*100, len([x for x in annotationList if x == "happy"])/len(annotationList)*100, len([x for x in annotationList if x == "neutral"])/len(annotationList)*100, len([x for x in annotationList if x == "sad"])/len(annotationList)*100, len([x for x in annotationList if x == "surprised"])/len(annotationList)*100, len(annotationList)]

for model in modelDict:
    fullModelList = []
    for author in modelDict[model]:
        fullModelList.extend(modelDict[model][author])
    weightedClassDistributionsDf.loc[len(weightedClassDistributionsDf)] = [model, len([x for x in fullModelList if x == "angry"])/len(fullModelList)*100, len([x for x in fullModelList if x == "disgusted"])/len(fullModelList)*100, len([x for x in fullModelList if x == "fearful"])/len(fullModelList)*100, len([x for x in fullModelList if x == "happy"])/len(fullModelList)*100, len([x for x in fullModelList if x == "neutral"])/len(fullModelList)*100, len([x for x in fullModelList if x == "sad"])/len(fullModelList)*100, len([x for x in fullModelList if x == "surprised"])/len(fullModelList)*100, len(fullModelList)]
weightedClassDistributionsDf.loc[len(weightedClassDistributionsDf)] = ["goldstandard", len([x for x in fullAnnotationList if x == "angry"])/len(fullAnnotationList)*100, len([x for x in fullAnnotationList if x == "disgusted"])/len(fullAnnotationList)*100, len([x for x in fullAnnotationList if x == "fearful"])/len(fullAnnotationList)*100, len([x for x in fullAnnotationList if x == "happy"])/len(fullAnnotationList)*100, len([x for x in fullAnnotationList if x == "neutral"])/len(fullAnnotationList)*100, len([x for x in fullAnnotationList if x == "sad"])/len(fullAnnotationList)*100, len([x for x in fullAnnotationList if x == "surprised"])/len(fullAnnotationList)*100, len(fullAnnotationList)]

emotionDf = emotionDf.sort_values(by="f1", ascending=False)
print(emotionDf[["emotion", "author", "model", "precision", "recall", "f1", "instances"]].to_string(index=False))
emotionDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/fairyTalesEmotionPerformance.csv", sep=",", index=False, encoding="utf-8")
print("\n")

weightedEmotionDf = weightedEmotionDf.sort_values(by="f1", ascending=False)
print(weightedEmotionDf[["emotion", "model", "precision", "recall", "f1", "instances"]].to_string(index=False))
weightedEmotionDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/fairyTalesweightedEmotionPerformance.csv", sep=",", index=False, encoding="utf-8")
print("\n")

modelDf = modelDf.sort_values(by="f1", ascending=False)
print(modelDf[["model", "author", "accuracy", "precision", "recall", "f1", "instances"]].to_string(index=False))
modelDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/fairyTalesModelPerformance.csv", sep=",", index=False, encoding="utf-8")
print("\n")

weightedModelsDf = weightedModelsDf.sort_values(by="f1", ascending=False)
print(weightedModelsDf[["model", "accuracy", "precision", "recall", "f1", "instances"]].to_string(index=False))
weightedModelsDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/fairyTalesWeightedModelPerformance.csv", sep=",", index=False, encoding="utf-8")
print("\n")

classDistributionsDf = classDistributionsDf.sort_values(by="author", ascending=False)
print(classDistributionsDf[["model", "author", "angry", "disgusted", "fearful", "happy", "neutral", "sad", "surprised", "instances"]].to_string(index=False))
classDistributionsDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/fairyTalesClassDistributions.csv", sep=",", index=False, encoding="utf-8")
print("\n")

weightedClassDistributionsDf = weightedClassDistributionsDf.sort_values(by="happy", ascending=False)
print(weightedClassDistributionsDf[["model", "angry", "disgusted", "fearful", "happy", "neutral", "sad", "surprised"]].to_string(index=False))
weightedClassDistributionsDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/fairyTalesWeightedClassDistributions.csv", sep=",", index=False, encoding="utf-8")
