import csv

dataFolder = "/home/rudolph/repos/masterarbeit"
goldenCsv = dataFolder + "/Skripte/Analyse/Modeldata/QualtricsSurvey.csv"
with open(goldenCsv, "r") as file:
    data = list(csv.reader(file,delimiter=","))
goldenList = []
for i in range(1, len(data)):
    responsesSentence = 8
    responsesContext = 10
    sentenceIndex = responsesContext
    lineValues = (data[i][sentenceIndex]).replace("[","").replace("]","")
    lineValues = [int(x) for x in lineValues.split(",")]
    neutralLenght = len([x for x in lineValues if x == 3])
    positiveLenght = len([x for x in lineValues if x in [1,2]])
    negativeLenght = len([x for x in lineValues if x in [4,5]])
    goldenList.append((neutralLenght/len(lineValues), positiveLenght/len(lineValues), negativeLenght/len(lineValues)))   

neutrals = [x[0] for x in goldenList if x[0] > x[1] and x[0] > x[2]]
positives = [x[1] for x in goldenList if x[1] > x[0] and x[1] > x[2]]
negatives = [x[2] for x in goldenList if x[2] > x[0] and x[2] > x[1]]

#calculate mean for neutrals, positives and negatives
neutralMean = sum(neutrals)/len(neutrals)*100
positiveMean = sum(positives)/len(positives)*100
negativeMean = sum(negatives)/len(negatives)*100
print("means of neural, positive and negative: ", neutralMean, positiveMean, negativeMean)

#calculate mean of neutral, positive and negative together
totalMean = (sum(neutrals)+sum(positives)+sum(negatives))/(len(neutrals)+len(positives)+len(negatives))*100
print("mean of all classes: ", totalMean)


