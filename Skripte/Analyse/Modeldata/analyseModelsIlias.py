import json
import numpy as np
import matplotlib.pyplot as plt
import glob
import pandas as pd
import csv
from scipy.stats import pearsonr
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, classification_report

dataFolder = "/home/rudolph/repos/masterarbeit"
goldenCsv = dataFolder + "/Skripte/Analyse/Modeldata/sentiment_Homer_Buch1_bereinigt.csv"
with open(goldenCsv, "r") as file:
    data = list(csv.reader(file,delimiter=","))
goldenList = []
for i in range(1, len(data)):
    lineValues = (data[i][5]).replace("[","").replace("]","")
    lineValues = [float(x) for x in lineValues.split(",")]
    if lineValues[3] > max(lineValues[0], lineValues[1], lineValues[2]): # >0.0 if narrator class is to be excluded even if other classes yield higher values 
        goldenList.append(None)
    else:
        if lineValues[0] > max(lineValues[1], lineValues[2]):
            goldenList.append(0) #goldenCsv values are neutral, positive, negative
        elif lineValues[1] > max(lineValues[0], lineValues[2]):
            goldenList.append(1)
        elif lineValues[2] > max(lineValues[0], lineValues[1]):
            goldenList.append(-1)
        # Für den Fall, dass zwei Klassen gleich groß sind, wird die Zeile nicht berücksichtigt
        else: 
            goldenList.append(None)
goldenListValids = [x for x in goldenList if x != None]
goldMean, goldMedian, goldStd = np.mean(goldenListValids), np.median(goldenListValids), np.std(goldenListValids)
goldPos = len([x for x in goldenList if x == 1])
goldNeg = len([x for x in goldenList if x == -1])
goldNeu = len([x for x in goldenList if x == 0]) 
goldTotal = goldPos + goldNeg + goldNeu
#print("Verteilung Goldstandard: Total/Pos/Neg/Neu")
#print(goldTotal, goldPos, goldNeg, goldNeu)
goldDist= str(int(goldPos/goldTotal*100))+"/"+str(int(goldNeg/goldTotal*100))+"/"+str(int(goldNeu/goldTotal*100))

def generateSentList(fileName):
    with open(fileName) as f:
        data = json.load(f)
        data = data["Book1"]
        sentList = []
        for line in data:
            #print(data[line])
            if data[line][0][1] != None:
                lineValues = data[line][0][1][0]
            else:
                lineValues = None
            sentList.append(lineValues)
    return sentList

def calculateEmotionMetrics(metricsDict, emotion, annotationList):
    emotionInstances = len([x for x in annotationList if x == int(emotion)])
    emotionPrecision = metricsDict[emotion]["precision"]
    emotionRecall = metricsDict[emotion]["recall"]
    emotionF1 = metricsDict[emotion]["f1-score"]
    return emotionInstances, emotionPrecision, emotionRecall, emotionF1

df = pd.DataFrame(columns=["model", "translation", "accuracy", "precision", "recall", "f1", "%POS/NEG/NEU"]) #, "corr", "mean", "median", "std"
ClassesDf = pd.DataFrame(columns=["model", "translation", "sentiment", "instances", "precision", "recall", "f1"]) 

emotionDict = {"-1": "negative", "0": "neutral", "1": "positive"}

sentFiles = sorted(glob.glob(dataFolder + "/Skripte/Analyse/Modeldata/sentDict/Iliaden/*.json"))
sentFiles = [x for x in sentFiles if "emotion" not in x]
for file in sentFiles:
    #print(file)
    model = file.split("Iliad_")[1].split("_sentDict.json")[0] #file.split("_")[2][0:10]
    model = model.split("-")[0] if model != "j-hartmann-sentiment-roberta-large-english-3-classes" else "j-hartmann"
    translation = file.split("_")[0].split("/")[-1]
    sentList = generateSentList(file)
    #print("Länge sentList")    
    #print(len(sentList))
    sentGolden = [x for x in goldenList]
    #print(len(sentList), len(goldenList))
    for i in range(len(sentList)):
        #print(i)
        if None in [sentGolden[i], sentList[i]]:
            sentList[i], sentGolden[i] = None, None
    sentGolden = [x for x in sentGolden if x != None]
    sentList = [x for x in sentList if x != None]
    #print("Länge bereinigte goldList: ", len(sentGolden))
    #print("Länge bereinigte sentList: ", len(sentList))
    accuracy = accuracy_score(sentGolden, sentList)
    precision = precision_score(sentGolden, sentList, average="weighted", zero_division=0)
    recall = recall_score(sentGolden, sentList, average="weighted", zero_division=0)
    f1 = f1_score(sentGolden, sentList, average="weighted", zero_division=0)
    corr = pearsonr(sentGolden, sentList)[0] #tuple of (correlation, p-value)
    mean, median, std = np.mean(sentList), np.median(sentList), np.std(sentList)
    positives = len([x for x in sentList if x == 1])
    negatives = len([x for x in sentList if x == -1])
    neutrals = len([x for x in sentList if x == 0])
    totals = positives + negatives + neutrals
    cldist = str(int(positives/totals*100))+"/"+str(int(negatives/totals*100))+"/"+str(int(neutrals/totals*100)) 
    df.loc[len(df)] = [model, translation, accuracy, precision, recall, f1, cldist] #, corr, mean, median, std
    metricsDict = classification_report(sentGolden, sentList, output_dict=True, zero_division=0)
    for emotion in ["-1", "0", "1"]:
        emotionInstances, emotionPrecision, emotionRecall, emotionF1 = calculateEmotionMetrics(metricsDict, emotion, sentGolden)
        ClassesDf.loc[len(ClassesDf)] = [model, translation, emotionDict[emotion], emotionInstances, emotionPrecision, emotionRecall, emotionF1]

zeroShotModels = ["facebook", "roberta", "valhalla", "MoritzLaurer"]

df = df.sort_values(by="f1", ascending=False)
pd.set_option("display.max_rows", None, "display.max_columns", None)
# drop "%POS/NEG/NEU" from df
df = df.drop(["%POS/NEG/NEU"], axis=1)

print(df[["model", "translation", "accuracy", "precision", "recall", "f1"]].to_string(index=False)) #, "corr", "mean", "median", "std"
#df.to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/Ilias.csv", sep=",", index=False, encoding="utf-8")
print("\n")

# just non zeroShot Models
dfNoZero = df[~df["model"].isin(zeroShotModels)]
dfNoZero = dfNoZero.sort_values(by="f1", ascending=False)
print(dfNoZero[["model", "translation", "accuracy", "precision", "recall", "f1"]].to_string(index=False)) #, "corr", "mean", "median", "std"
dfNoZero.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasNoZero.csv", sep=",", index=False, encoding="utf-8")
print("\n")

# just zeroShot Models
dfZero = df[df["model"].isin(zeroShotModels)]
dfZero = dfZero.sort_values(by="f1", ascending=False)
print(dfZero[["model", "translation", "accuracy", "precision", "recall", "f1"]].to_string(index=False)) #, "corr", "mean", "median", "std"
dfZero.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasZero.csv", sep=",", index=False, encoding="utf-8")
print("\n")

df2 = pd.DataFrame(columns=["model", "accuracy", "precision", "recall", "f1"])
for model in df["model"].unique():
    df2.loc[len(df2)] = [model, np.mean(df[df["model"] == model]["accuracy"]), np.mean(df[df["model"] == model]["precision"]), np.mean(df[df["model"] == model]["recall"]), np.mean(df[df["model"] == model]["f1"])]
df2 = df2.sort_values(by="f1", ascending=False)
print(df2.to_string(index=False))
#df2.to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasMean.csv", sep=",", index=False, encoding="utf-8")
print("\n")

dfMeanNoZero = pd.DataFrame(columns=["model", "accuracy", "precision", "recall", "f1"])
for model in dfNoZero["model"].unique():
    dfMeanNoZero.loc[len(dfMeanNoZero)] = [model, np.mean(dfNoZero[dfNoZero["model"] == model]["accuracy"]), np.mean(dfNoZero[dfNoZero["model"] == model]["precision"]), np.mean(dfNoZero[dfNoZero["model"] == model]["recall"]), np.mean(dfNoZero[dfNoZero["model"] == model]["f1"])]
dfMeanNoZero = dfMeanNoZero.sort_values(by="f1", ascending=False)
print(dfMeanNoZero.to_string(index=False))
dfMeanNoZero.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasMeanNoZero.csv", sep=",", index=False, encoding="utf-8")

dfMeanZero = pd.DataFrame(columns=["model", "accuracy", "precision", "recall", "f1"])
for model in dfZero["model"].unique():
    dfMeanZero.loc[len(dfMeanZero)] = [model, np.mean(dfZero[dfZero["model"] == model]["accuracy"]), np.mean(dfZero[dfZero["model"] == model]["precision"]), np.mean(dfZero[dfZero["model"] == model]["recall"]), np.mean(dfZero[dfZero["model"] == model]["f1"])]
dfMeanZero = dfMeanZero.sort_values(by="f1", ascending=False)
dfMeanZero = dfMeanZero.round(2)
print(dfMeanZero.to_string(index=False))
dfMeanZero.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasMeanZero.csv", sep=",", index=False, encoding="utf-8")

ClassesDf = ClassesDf.sort_values(by="f1", ascending=False)
ClassesDf = ClassesDf.round(2)
pd.set_option("display.max_rows", None, "display.max_columns", None)

# remove models in ClassesDf that are not in zeroShotModels
print(ClassesDf[["model", "translation", "sentiment", "instances", "precision", "recall", "f1"]].to_string(index=False))
print("\n")

ClassesZeroDf = ClassesDf[ClassesDf["model"].isin(zeroShotModels)]
print(ClassesZeroDf[["model", "translation", "sentiment", "instances", "precision", "recall", "f1"]].to_string(index=False))
ClassesZeroDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasClassesZero.csv", sep=",", index=False, encoding="utf-8")
print("\n")

# remove models in ClassesDf that are in zeroShotModels
ClassesNoZeroDf = ClassesDf[~ClassesDf["model"].isin(zeroShotModels)]
print(ClassesNoZeroDf[["model", "translation", "sentiment", "instances", "precision", "recall", "f1"]].to_string(index=False))
ClassesNoZeroDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasClassesNoZero.csv", sep=",", index=False, encoding="utf-8")
print("\n")

ClassesMeanZeroDf = pd.DataFrame(columns=["model", "sentiment", "precision", "recall", "f1"])
# calculate mean metrics for each sentiment for each zeroshot model
for model in zeroShotModels:
    for sentiment in ["positive", "negative", "neutral"]:
        ClassesMeanZeroDf.loc[len(ClassesMeanZeroDf)] = [model, sentiment, np.mean(ClassesZeroDf[(ClassesZeroDf["model"] == model) & (ClassesZeroDf["sentiment"] == sentiment)]["precision"]), np.mean(ClassesZeroDf[(ClassesZeroDf["model"] == model) & (ClassesZeroDf["sentiment"] == sentiment)]["recall"]), np.mean(ClassesZeroDf[(ClassesZeroDf["model"] == model) & (ClassesZeroDf["sentiment"] == sentiment)]["f1"])]
print(ClassesMeanZeroDf.to_string(index=False))
ClassesMeanZeroDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasClassesMeanZero.csv", sep=",", index=False, encoding="utf-8")
print("\n")

ClassesMeanNoZeroDf = pd.DataFrame(columns=["model", "sentiment", "precision", "recall", "f1"])
# calculate mean metrics for each sentiment for each non-zeroshot model
for model in dfNoZero["model"].unique():
    for sentiment in ["positive", "negative", "neutral"]:
        ClassesMeanNoZeroDf.loc[len(ClassesMeanNoZeroDf)] = [model, sentiment, np.mean(ClassesNoZeroDf[(ClassesNoZeroDf["model"] == model) & (ClassesNoZeroDf["sentiment"] == sentiment)]["precision"]), np.mean(ClassesNoZeroDf[(ClassesNoZeroDf["model"] == model) & (ClassesNoZeroDf["sentiment"] == sentiment)]["recall"]), np.mean(ClassesNoZeroDf[(ClassesNoZeroDf["model"] == model) & (ClassesNoZeroDf["sentiment"] == sentiment)]["f1"])]
print(ClassesMeanNoZeroDf.to_string(index=False))
ClassesMeanNoZeroDf.round(2).to_csv(dataFolder + "/Skripte/Analyse/Modeldata/Modelperformance/iliasClassesMeanNoZero.csv", sep=",", index=False, encoding="utf-8")


