import json
import numpy as np
import os
import pandas as pd
from hurst import compute_Hc
from sklearn.metrics import accuracy_score
from scipy.stats import pearsonr

def generateSentList(valueHolder, outputList):
    if type(valueHolder) == dict:
        for bookKey, bookValue in valueHolder.items():
            if type(bookValue) == dict:
                for chapterKey, chapterValue in bookValue.items():
                    for sentTuple in chapterValue:
                        outputList.append(sentTuple)
            elif type(bookValue) == list:
                for sentTuple in bookValue:
                    outputList.append(sentTuple)   
            else:
                raise Exception("Unknown type in generateSentList")     
    elif type(valueHolder) == list:
        for element in valueHolder:
            if type(element) == dict:
                for elementKey, elementValue in element.items():
                    for sentTuple in elementValue:
                        outputList.append(sentTuple)
            elif type(element) == list:
                outputList.append(element)    
            else:
                raise Exception("Unknown type in generateSentList")    
    return(outputList)

def extractParticularEmotion(particularEmotion, emotionList, threshold, usePropability):
    particularEmotionList = []
    for sentence in emotionList:
        for emotion in sentence[1]:
            if emotion[0] == particularEmotion:
                if usePropability:
                    particularEmotionList.append(emotion[1])
                else:
                    if emotion[1] > threshold:
                        particularEmotionList.append(1)
                    else:
                        particularEmotionList.append(0)
    return particularEmotionList

def extractHighestEmotion(particularEmotion, emotionList, threshold, usePropability):
    particularEmotionList = []
    for sentence in emotionList:
        if usePropability:
            if sentence[1][0][0] == particularEmotion:
                particularEmotionList.append(sentence[1][0][1])
        else:
            if sentence[1][0][0] == particularEmotion:
                particularEmotionList.append(1)
            else:
                particularEmotionList.append(0)
    return particularEmotionList

def generateSectionList(fileDict):
    bookList, chapterList, bookStrings, chapterStrings = [], [], [], []
    for key, value in fileDict.items():
        bookList.append(value)
        bookStrings.append(key)
        if type(value) == dict:
            for chapterKey, chapterValue in value.items():
                chapterList.append(chapterValue)
                chapterStrings.append(key+chapterKey)
    return bookList, chapterList, bookStrings, chapterStrings

def calculateSentStats(sentList):
    mean = np.mean(sentList)
    positives = len([x for x in sentList if x == 1])
    negatives = len([x for x in sentList if x == -1])
    neutrals = len([x for x in sentList if x == 0])
    totals = len(sentList)
    cldist = str(int(positives/totals*100))+"/"+str(int(negatives/totals*100))+"/"+str(int(neutrals/totals*100))
    return mean, cldist

def cumulateSeries(series):
    cumulated = [series[0]]
    for i in range(1, len(series)):
        cumulated.append(cumulated[-1] + series[i])
    return cumulated

def hurstExponent(series):
    try:
        H, c, data = compute_Hc(series, kind='change', simplified=True)
    except:
        H = None
    return H

def ensembleValueCalculation(valueList):
    #value = (max(set(valueList), key=valueList.count))
    value = np.mean(valueList)
    return value

def createSentStatDict(rootDir, threshold, usePropability, sentimentModels):
    sentDict = {}
    for subdir, dirs, files in os.walk(rootDir):
        for file in files:
            filePath = os.path.join(subdir, file)
            fileData = file.split("/")[-1].split("_")
            author, title, genre = fileData[0], fileData[1], subdir.split("/")[-1]
            if len(fileData) > 4:
                model = fileData[2]+"_"+fileData[3]
            else:
                model = fileData[2]
            if model in sentimentModels:
                with open(filePath) as f:
                    fileDict = json.load(f)
                # Beim Aufbau der Daten wurden die Exzerpte durch einen Schreibfehler nicht entfernt
                if "Dionysios of Halikarnassos_Roman Antiquities" in file:
                    fileDict = dict(list(fileDict.items())[0:11])
                if not title+"_"+author in sentDict:
                    sentDict[title+"_"+author] = {}
                title = title+"_"+author
                sentDict[title]["genre"] = genre
                sentDict[title]["author"] = author
                sentList = [x[1][0] for x in generateSentList(fileDict, [])]
                meanSentiment, cldist = calculateSentStats(sentList)
                bookList, chapterList, bookStrings, chapterStrings =  generateSectionList(fileDict)
                bookSentList = [[x[1][0] for x in generateSentList(book, [])] for book in bookList] if bookList else None
                chapterSentList = [[x[1][0] for x in generateSentList(chapter, [])] for chapter in chapterList] if chapterList else None
                sentDict[title]["sentences"] = len(sentList)
                if not model in sentDict[title]:
                    sentDict[title][model] = {}
                sentDict[title][model]["sentimentMean"] = meanSentiment
                sentDict[title][model]["clDist"] = cldist
                sentDict[title][model]["sentList"] = sentList
                sentDict[title][model]["cumulatedSentList"] = cumulateSeries(sentList)
                sentDict[title][model]["hurst"] = hurstExponent(sentList)
                sentDict[title][model]["bookSentList"] = bookSentList
                sentDict[title][model]["cumulatedBookSentList"] = [cumulateSeries(x) for x in bookSentList] if bookSentList else None
                sentDict[title][model]["bookSentMeans"] = [np.mean(x) for x in bookSentList] if bookSentList else None
                sentDict[title][model]["bookHursts"] = [hurstExponent(x) for x in bookSentList] if bookSentList else None
                sentDict[title][model]["chapterSentList"] = chapterSentList
                sentDict[title][model]["cumulatedChapterSentList"] = [cumulateSeries(x) for x in chapterSentList] if chapterSentList else None
                sentDict[title][model]["chapterSentMeans"] = [np.mean(x) for x in chapterSentList] if chapterSentList else None
                sentDict[title][model]["chapterHursts"] = [hurstExponent(x) for x in chapterSentList] if chapterSentList else None
                sentDict[title]["bookStrings"] = bookStrings
                sentDict[title]["chapterStrings"] = chapterStrings
            if model == "j-hartmann-emotion-english-roberta-large":
                with open(filePath) as f:
                    fileDict = json.load(f)
                # Beim Aufbau der Daten wurden die Exzerpte durch einen Schreibfehler nicht entfernt
                if "Dionysios of Halikarnassos_Roman Antiquities" in file:
                    fileDict = dict(list(fileDict.items())[0:11])
                if not title+"_"+author in sentDict:
                    sentDict[title+"_"+author] = {}
                title = title+"_"+author
                emotionDict = {}
                emotionList = generateSentList(fileDict, [])
                bookList, chapterList, bookStrings, chapterStrings =  generateSectionList(fileDict)
                bookEmotionList = [generateSentList(book, []) for book in bookList] if bookList else None
                chapterEmotionList = [generateSentList(chapter, []) for chapter in chapterList] if chapterList else None
                labels = ["anger", "disgust", "fear", "joy", "sadness", "surprise", "neutral"]
                for label in labels:
                    emotionDict[label] = {}
                    emotionDict[label]["emotionList"] = extractHighestEmotion(label, emotionList, threshold, usePropability)
                    emotionDict[label]["emotionMean"] = np.mean(emotionDict[label]["emotionList"])
                    emotionDict[label]["bookEmotionList"] = [extractHighestEmotion(label, book, threshold, usePropability) for book in bookEmotionList] if bookEmotionList else None
                    emotionDict[label]["bookEmotionMeans"] = [np.mean(x) for x in emotionDict[label]["bookEmotionList"]] if bookEmotionList else None
                    emotionDict[label]["chapterEmotionList"] = [extractHighestEmotion(label, chapter, threshold, usePropability) for chapter in chapterEmotionList] if chapterEmotionList else None
                    emotionDict[label]["chapterEmotionMeans"] = [np.mean(x) for x in emotionDict[label]["chapterEmotionList"]] if chapterEmotionList else None
                emotionDict["emotionality"] = 1 - emotionDict["neutral"]["emotionMean"]
                emotionDict["suffering"] = emotionDict["anger"]["emotionMean"] + emotionDict["sadness"]["emotionMean"] + emotionDict["fear"]["emotionMean"] + emotionDict["disgust"]["emotionMean"]
                emotionDict["bookEmotionality"] = [1 - x for x in emotionDict["neutral"]["bookEmotionMeans"]] if bookEmotionList else None
                emotionDict["bookSuffering"] = [w + x + y + z for w, x, y, z in zip(emotionDict["anger"]["bookEmotionMeans"], emotionDict["disgust"]["bookEmotionMeans"], emotionDict["fear"]["bookEmotionMeans"], emotionDict["sadness"]["bookEmotionMeans"])] if bookEmotionList else None
                emotionDict["chapterEmotionality"] = [1 - x for x in emotionDict["neutral"]["chapterEmotionMeans"]] if chapterEmotionList else None
                emotionDict["chapterSuffering"] = [w + x + y + z for w, x, y, z in zip(emotionDict["anger"]["chapterEmotionMeans"], emotionDict["disgust"]["chapterEmotionMeans"], emotionDict["fear"]["chapterEmotionMeans"], emotionDict["sadness"]["chapterEmotionMeans"])] if chapterEmotionList else None
                sentDict[title]["emotionDict"] = emotionDict
    for title in sentDict:
        sentLists = [sentDict[title][model]["sentList"] for model in ensembleModels if model in sentDict[title]]
        ensembleSentList = []
        for i in range(len(sentLists[0])):
            sentValues = [sentList[i] for sentList in sentLists]
            ensembleSentList.append(ensembleValueCalculation(sentValues))
        bookSentLists = [sentDict[title][model]["bookSentList"] for model in ensembleModels if model in sentDict[title]]
        if bookSentLists[0] != None:
            ensembleBookSentList = []
            for i in range(len(bookSentLists[0])):
                singleBookSentList = []
                for j in range(len(bookSentLists[0][i])):
                    sentValues = [sentList[i][j] for sentList in bookSentLists]
                    singleBookSentList.append(ensembleValueCalculation(sentValues))
                ensembleBookSentList.append(singleBookSentList)
        else:
            ensembleBookSentList = None
        chapterSentLists = [sentDict[title][model]["chapterSentList"] for model in ensembleModels if model in sentDict[title]]
        if chapterSentLists[0] != None:
            ensembleChapterSentList = []
            for i in range(len(chapterSentLists[0])):
                singleChapterSentList = []
                for j in range(len(chapterSentLists[0][i])):
                    sentValues = [sentList[i][j] for sentList in chapterSentLists]
                    singleChapterSentList.append(ensembleValueCalculation(sentValues))
                ensembleChapterSentList.append(singleChapterSentList)
        else:
            ensembleChapterSentList = None
        meanSentiment, cldist = calculateSentStats(ensembleSentList)
        sentDict[title]["ensemble"] = {}
        sentDict[title]["ensemble"]["sentimentMean"] = meanSentiment
        sentDict[title]["ensemble"]["clDist"] = cldist
        sentDict[title]["ensemble"]["sentList"] = ensembleSentList
        sentDict[title]["ensemble"]["cumulatedSentList"] = cumulateSeries(ensembleSentList)
        sentDict[title]["ensemble"]["hurst"] = hurstExponent(ensembleSentList)
        sentDict[title]["ensemble"]["bookSentList"] = ensembleBookSentList
        sentDict[title]["ensemble"]["cumulatedBookSentList"] = [cumulateSeries(x) for x in ensembleBookSentList] if ensembleBookSentList else None
        sentDict[title]["ensemble"]["bookSentMeans"] = [np.mean(x) for x in ensembleBookSentList] if ensembleBookSentList else None
        sentDict[title]["ensemble"]["bookHursts"] = [hurstExponent(x) for x in ensembleBookSentList] if ensembleBookSentList else None
        sentDict[title]["ensemble"]["chapterSentList"] = ensembleChapterSentList
        sentDict[title]["ensemble"]["cumulatedChapterSentList"] = [cumulateSeries(x) for x in ensembleChapterSentList] if ensembleChapterSentList else None
        sentDict[title]["ensemble"]["chapterSentMeans"] = [np.mean(x) for x in ensembleChapterSentList] if ensembleChapterSentList else None
        sentDict[title]["ensemble"]["chapterHursts"] = [hurstExponent(x) for x in ensembleChapterSentList] if ensembleChapterSentList else None

    for title in sentDict:
        for model in totalModels:
            sentDict[title][model]["tragicness"] = -1/2 * sentDict[title][model]["sentimentMean"] + 1/4 * sentDict[title]["emotionDict"]["emotionality"] + 1/4 * sentDict[title]["emotionDict"]["suffering"]
            sentDict[title][model]["bookTragicness"] = [-1/2 * x + 1/4 * y + 1/4 * z for x, y, z in zip(sentDict[title][model]["bookSentMeans"], sentDict[title]["emotionDict"]["bookEmotionality"], sentDict[title]["emotionDict"]["bookSuffering"])] if sentDict[title][model]["bookSentMeans"] else None
            sentDict[title][model]["chapterTragicness"] = [-1/2 * x + 1/4 * y + 1/4 * z for x, y, z in zip(sentDict[title][model]["chapterSentMeans"], sentDict[title]["emotionDict"]["chapterEmotionality"], sentDict[title]["emotionDict"]["chapterSuffering"])] if sentDict[title][model]["chapterSentMeans"] else None
    with open(dataFolder + "/Skripte/Analyse/sentData/sentData.json", "w") as f:
        json.dump(sentDict, f)

dataFolder = "/home/rudolph/repos/masterarbeit"
rootDir = f"{dataFolder}/Skripte/sentDict/"
threshold = 0.5
usePropability = False
sentimentModels = ["siebert-sentiment-roberta-large-english", "roberta-large-mnli2Classes", "facebook-bart-large-mnli2Classes"] 
ensembleModels = ["siebert-sentiment-roberta-large-english", "facebook-bart-large-mnli2Classes", "roberta-large-mnli2Classes"] 
totalModels = sentimentModels + ["ensemble"]
#createSentStatDict(rootDir, threshold, usePropability, sentimentModels)

with open(dataFolder + "/Skripte/Analyse/sentData/sentData.json") as f:
    sentDict = json.load(f)
sentCompareDf = pd.DataFrame(columns=["title", "genre", "model", "sentiment"])
for sentimentModel in sentimentModels + ["ensemble"]:
    df = pd.DataFrame(columns=["title", "genre", "sentences", "sentiment", "hurst", "anger", "disgust", "fear", "joy", "sadness", "surprise", "neutral", "emotionality", "suffering", "tragicness"])
    for key, value in sentDict.items():
        df.loc[len(df)] = [key, value["genre"], value["sentences"], value[sentimentModel]["sentimentMean"], value[sentimentModel]["hurst"], value["emotionDict"]["anger"]["emotionMean"], value["emotionDict"]["disgust"]["emotionMean"], value["emotionDict"]["fear"]["emotionMean"], value["emotionDict"]["joy"]["emotionMean"], value["emotionDict"]["sadness"]["emotionMean"], value["emotionDict"]["surprise"]["emotionMean"], value["emotionDict"]["neutral"]["emotionMean"], value["emotionDict"]["emotionality"], value["emotionDict"]["suffering"], value[sentimentModel]["tragicness"]]
        sentCompareDf.loc[len(sentCompareDf)] = [key, value["genre"], sentimentModel, value[sentimentModel]["sentimentMean"]]
    df = df.drop(columns=["hurst"])
    df = df.sort_values(by=["genre", "tragicness"], ascending=True)
    df = df.round(2)
    df[["title", "genre", "sentiment", "emotionality", "suffering", "tragicness"]].to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/labelValuesWorks.csv", index=False)

    df2 = df.drop(columns=["title"])
    df2 = df2.drop(columns=["sentences"])
    df2 = df2.groupby("genre").mean()
    df2 = df2.sort_values(by=["genre"], ascending=True)
    df2 = df2.round(2)
    df2[["sentiment", "emotionality", "suffering", "tragicness"]].to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/labelValuesGenre.csv", index=True)

    df3 = df.drop(columns=["sentences", "emotionality", "suffering", "tragicness"])
    df3 = df3.sort_values(by=["genre", "sentiment"], ascending=True)
    df3.to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/labelValuesWorksPure.csv", index=False)

    df4 = df2.drop(columns=["emotionality", "suffering", "tragicness"])
    df4 = df4.sort_values(by=["sentiment"], ascending=True)
    df4.to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/labelValuesGenrePure.csv", index=True)

sentCompareDf = sentCompareDf.round(2)
sentCompareDf = sentCompareDf.sort_values(by=["title"], ascending=True)
sentCompareDf.to_csv(dataFolder + f"/Skripte/Analyse/sentData/sentCompare.csv", index=False)
sentCompareDf = sentCompareDf.drop(columns=["title"])
sentCompareDf = sentCompareDf.groupby([ "genre", "model"]).mean()
sentCompareDf = sentCompareDf.sort_values(by=["genre", "model"], ascending=True)
sentCompareDf = sentCompareDf.round(2)
sentCompareDf.to_csv(dataFolder + f"/Skripte/Analyse/sentData/sentCompareGenre.csv", index=True)

# calculate correlations between sentiment models
sentimentDict = {}
for model in sentimentModels + ["ensemble"]:
    sentimentDict[model] = []
    for key, value in sentDict.items():
        sentimentDict[model].append(value[model]["sentimentMean"])
sieberSentiments = sentimentDict["siebert-sentiment-roberta-large-english"]
robertaSentiments = sentimentDict["roberta-large-mnli2Classes"]
facebookSentiments = sentimentDict["facebook-bart-large-mnli2Classes"]
ensembleSentiments = sentimentDict["ensemble"]
print("Correlation between Siebert and Roberta:", pearsonr(sieberSentiments, robertaSentiments))
print("Correlation between Siebert and Facebook:", pearsonr(sieberSentiments, facebookSentiments))
print("Correlation between Roberta and Facebook:", pearsonr(robertaSentiments, facebookSentiments))
for model in sentimentModels:
    print("Correlation between", model, "and ensemble:", pearsonr(sentimentDict[model], ensembleSentiments))
print("\n")

# calculate relationship between sentiment and specific emotions
labels = ["anger", "disgust", "fear", "joy", "sadness", "surprise", "neutral"]
for model in sentimentModels:
    print(model)
    for label in labels:
        disgustList = []
        sentimentList = []
        for title in sentDict:
            sentimentList.extend(sentDict[title][model]["sentList"])
            disgustList.extend(sentDict[title]["emotionDict"][label]["emotionList"])
        disgusted = []
        polar = []
        for i in range(len(sentimentList)):
            if disgustList[i] == 1:
                disgusted.append(-1)
                polar.append(sentimentList[i])
        print(label, accuracy_score(disgusted, polar))
    print("\n")

# create tables for history and tragedy works, books and chapters
def calcTragicness(title, author, genre, sentiment, emotionality, suffering, tragicness, df):
    df = df._append({"title": title, "author": author, "genre": genre, "sentiment": sentiment, "emotionality": emotionality, "suffering": suffering, "tragicness": tragicness}, ignore_index=True)
    return df
for sentimentModel in sentimentModels + ["ensemble"]:
    tragicnessDf = pd.DataFrame(columns=["title", "author", "genre", "sentiment", "emotionality", "suffering", "tragicness"])
    for key, value in sentDict.items():
        if value["genre"] == "Tragödien":
            title, author = key.split("_")
            genre = value["genre"]
            tragicnessDf = calcTragicness(title, author, genre, value[sentimentModel]["sentimentMean"], value["emotionDict"]["emotionality"], value["emotionDict"]["suffering"], value[sentimentModel]["tragicness"], tragicnessDf)
            if value["bookStrings"]:
                for i in range(len(value["bookStrings"])):
                    genre = value["genre"] + "buch"
                    tragicnessDf = calcTragicness(title + "_" + value["bookStrings"][i], author, genre, value[sentimentModel]["bookSentMeans"][i], value["emotionDict"]["bookEmotionality"][i], value["emotionDict"]["bookSuffering"][i], value[sentimentModel]["bookTragicness"][i], tragicnessDf)
        elif value["genre"] == "Geschichtswerke":
            title, author = key.split("_")
            genre = value["genre"]
            tragicnessDf = calcTragicness(title, author, genre, value[sentimentModel]["sentimentMean"], value["emotionDict"]["emotionality"], value["emotionDict"]["suffering"], value[sentimentModel]["tragicness"], tragicnessDf)
            if value["bookStrings"]:
                for i in range(len(value["bookStrings"])):
                    genre = value["genre"] + "buch"
                    tragicnessDf = calcTragicness(title + "_" + value["bookStrings"][i], author, genre, value[sentimentModel]["bookSentMeans"][i], value["emotionDict"]["bookEmotionality"][i], value["emotionDict"]["bookSuffering"][i], value[sentimentModel]["bookTragicness"][i], tragicnessDf)
            if value["chapterStrings"]:
                for i in range(len(value["chapterStrings"])):
                    genre = value["genre"] + "kapitel"
                    tragicnessDf = calcTragicness(title + "_" + value["chapterStrings"][i], author, genre, value[sentimentModel]["chapterSentMeans"][i], value["emotionDict"]["chapterEmotionality"][i], value["emotionDict"]["chapterSuffering"][i], value[sentimentModel]["chapterTragicness"][i], tragicnessDf)
        elif value["genre"] == "Biographie":
            title, author = key.split("_")
            genre = value["genre"]
            if author == "Plutarch":
                PlutarchDf = pd.DataFrame(columns=["title", "author", "genre", "sentiment", "emotionality", "suffering", "tragicness"])
                if value["bookStrings"]:
                    for i in range(len(value["bookStrings"])):
                        genre = value["genre"] + "buch"
                        PlutarchDf = calcTragicness(title + "_" + value["bookStrings"][i], author, genre, value[sentimentModel]["bookSentMeans"][i], value["emotionDict"]["bookEmotionality"][i], value["emotionDict"]["bookSuffering"][i], value[sentimentModel]["bookTragicness"][i], PlutarchDf)

    PlutarchDf = PlutarchDf.sort_values(by=["tragicness"], ascending=True)
    PlutarchDf = PlutarchDf.round(2)
    PlutarchDf.to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/Plutarch.csv", index=False)

    tragicnessDf = tragicnessDf.sort_values(by=["tragicness"], ascending=True)
    tragicnessDf = tragicnessDf.round(2)
    pd.set_option("display.max_rows", None)

    workTragicnessDf = tragicnessDf[tragicnessDf["genre"] != "Geschichtswerkebuch"]
    workTragicnessDf = workTragicnessDf[workTragicnessDf["genre"] != "Geschichtswerkekapitel"]
    workTragicnessDf = workTragicnessDf[workTragicnessDf["genre"] != "Tragödienbuch"]
    workTragicnessDf = workTragicnessDf.reset_index(drop=True)
    workTragicnessDf.to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/workTragicness.csv", index=False)

    bookTragicnessDf = tragicnessDf[tragicnessDf["genre"] != "Geschichtswerke"]
    bookTragicnessDf = bookTragicnessDf[bookTragicnessDf["genre"] != "Geschichtswerkekapitel"]
    bookTragicnessDf = bookTragicnessDf[bookTragicnessDf["genre"] != "Tragödienbuch"]
    bookTragicnessDf = bookTragicnessDf.reset_index(drop=True)
    bookTragicnessDf.to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/bookTragicness.csv", index=False)

    chapterTragicnessDf = tragicnessDf[tragicnessDf["genre"] != "Geschichtswerke"]
    chapterTragicnessDf = chapterTragicnessDf[chapterTragicnessDf["genre"] != "Geschichtswerkebuch"]
    chapterTragicnessDf = chapterTragicnessDf[chapterTragicnessDf["genre"] != "Tragödienbuch"]
    chapterTragicnessDf = chapterTragicnessDf[chapterTragicnessDf["genre"] != "Tragödien"]
    chapterTragicnessDf = chapterTragicnessDf.reset_index(drop=True)
    chapterTragicnessDf.to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/chapterTragicness.csv", index=False)

    TragedybookTragicnessDf = tragicnessDf[tragicnessDf["genre"] != "Geschichtswerk"]
    TragedybookTragicnessDf = TragedybookTragicnessDf[TragedybookTragicnessDf["genre"] != "Geschichtswerkekapitel"]
    TragedybookTragicnessDf = TragedybookTragicnessDf[TragedybookTragicnessDf["genre"] != "Tragödien"]
    TragedybookTragicnessDf = TragedybookTragicnessDf.reset_index(drop=True)
    TragedybookTragicnessDf.to_csv(dataFolder + f"/Skripte/Analyse/sentData/{sentimentModel}/tragedyBookTragicness.csv", index=False)