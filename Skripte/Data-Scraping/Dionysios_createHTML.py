from bs4 import BeautifulSoup as bs
import requests
import os

dataFolder = "/home/rudolph/repos/masterarbeit"
file = f"{dataFolder}/Skripte/Data-Scraping/DionysiusofHalicarnassus_Roman Antiquities.html"
url = "https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Dionysius_of_Halicarnassus/home.html"

if not os.path.isfile(file):
    html = requests.get(url)
    html.encoding = "utf-8"
    with open(file,"w") as f:
        f.write(html.text)

title = "Dionysius_of_Halicarnassus"

with open(file,"r") as f:
    soup = bs(f.read(), "html5lib")

ankerSoup = [a for a in soup.find_all(href=True) if title in a.get("href") and not "home" in a.get("href")]
urlList = []
baseUrl = "/".join(url.split("/")[0:-1])

for a in ankerSoup:
    urlList.append(baseUrl +"/" + a.get("href").split("/")[-1].rstrip())

if not os.path.isdir(f"{dataFolder}/Skripte/Data-Scraping/urls"):
    os.mkdir(f"{dataFolder}/Skripte/Data-Scraping/urls")
    
for url in urlList:
    if not os.path.isfile(f"{dataFolder}/Skripte/Data-Scraping/urls/"+url.split("/")[-1]):
        html = requests.get(url)
        html.encoding = "utf-8"
        with open(f"{dataFolder}/Skripte/Data-Scraping/urls"+url.split("/")[-1],"w") as f:
            f.write(html.text)