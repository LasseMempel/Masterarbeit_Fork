import json
import glob
import os
import random

dataFolder = "/home/rudolph/repos/masterarbeit"
rootDir = f"{dataFolder}/Skripte/Data-Scraping/literaryEmotions/affect tales/"
genre = "Märchen"
authors = ["Grimms", "HCAndersen", "Potter"]

emotionDict =  {"A": "angry", 
                "D": "disgusted", 
                "F": "fearful", 
                "H": "happy", 
                "N": "neutral", 
                "Sa": "sad", 
                "S": "sad", 
                "Su+": "surprised", 
                "+": "surprised", 
                "Su-": "surprised", 
                "-": "surprised"
                }

for author in authors:
    authorDict = {}
    annotationDict = {}
    lineCounter = 0
    for file in glob.glob(f"{rootDir}{author}/*.emmood"):
        with open(file, "r") as f:
            lines = f.readlines()
            for line in lines:
                line = line.split("\t")
                if emotionDict[line[1].split(":")[0]] == emotionDict[line[1].split(":")[1]] and line[1] == line[2].replace("S", "Sa").replace("+", "Su+").replace("-", "Su-"):
                    lineCounter += 1
                    authorDict["Line"+str(lineCounter)] = line[3]
                    annotationDict["Line"+str(lineCounter)] = emotionDict[line[1].split(":")[0]]
    valueDict = {"angry" : 0, "disgusted" : 0, "fearful" : 0, "happy" : 0, "neutral" : 0, "sad" : 0, "surprised" : 0}
    for line in annotationDict:
        valueDict[annotationDict[line]] += 1
    averageEmotionCount = int((len(annotationDict) - (valueDict["neutral"]))/6)
    neutralKeys = [x for x in annotationDict if annotationDict[x] == "neutral"]
    sampleList = random.sample(neutralKeys, valueDict["neutral"] - averageEmotionCount)
    for sample in sampleList:
        annotationDict.pop(sample)
        authorDict.pop(sample)
    if not os.path.exists(f"{dataFolder}/Skripte/sourceDict/{genre}"):
        os.makedirs(f"{dataFolder}/Skripte/sourceDict/{genre}")
    with open(f"{dataFolder}/Skripte/sourceDict/{genre}/{author}_AffectTales.json", "w") as f:
        json.dump(authorDict, f)
    with open(f"{dataFolder}/Skripte/Analyse/Modeldata/Märchen Annotationen/{author}_AffectTalesAnnotations.json", "w") as f:
        json.dump(annotationDict, f)