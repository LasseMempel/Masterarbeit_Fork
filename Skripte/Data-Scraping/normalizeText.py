import html
import re
def normalize(text):
    # Entfernen von Klammern und anderen Sonderzeichen
    text = re.sub(r"[\([{<>})\]]", "", text)
    # Entfernen von whitespace
    text = " ".join(text.split())
    
    return text