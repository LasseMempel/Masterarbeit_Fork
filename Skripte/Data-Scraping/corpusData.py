import os
import json
import pandas as pd
import nltk

dataFolder = "/home/rudolph/repos/masterarbeit"
rootDir = f"{dataFolder}/Skripte/sourceDict/"

def readDicts(rootDir):
    dictList = []
    dictNames = []
    genreList = []
    for subdir, dirs, files in os.walk(rootDir):
        for file in files:
            if not "Perseus" in file and not "Iliad" in file:
                filePath = os.path.join(subdir, file)
                with open(filePath, "r") as f:
                    dictList.append( json.load(f))
                dictNames.append(" ".join((file.split(".")[0]).split("_")))
                genreList.append(subdir.split("/")[-1])
    return dictList, dictNames, genreList

dictList, dictNames, genreList = readDicts(rootDir)

def readDictsRecursively(dictionary):
    stringList = []
    for key in dictionary:
        if isinstance(dictionary[key], dict):
            stringList.extend(readDictsRecursively(dictionary[key]))
        elif isinstance(dictionary[key], str):
            stringList.append(dictionary[key])
        else:
            raise TypeError("Dictionary values must be either strings or dictionaries")
    return stringList

df = pd.DataFrame(columns=["work", "genre", "sentences", "words", "books", "chapters", "sentences/book"])

allTexts = []
for dictionary, dictionaryName, genre in zip(dictList, dictNames, genreList):
    dictionaryStringList = readDictsRecursively(dictionary)
    dictionaryString = " ".join(dictionaryStringList)
    allTexts.append(dictionaryString)
    dictionarySentences = nltk.sent_tokenize(dictionaryString)
    dictionaryWords = nltk.word_tokenize(dictionaryString)
    dictionaryKeys = len(dictionary.keys())
    valueDictionaries = 0
    for key, value in dictionary.items():
        if isinstance(value, dict):
            valueDictionaries += len(dictionary[key])
    avgSentences = round(len(dictionarySentences) / dictionaryKeys if dictionaryKeys > 0 else 0)
    df = pd.concat([df, pd.DataFrame([[dictionaryName, genre, len(dictionarySentences), len(dictionaryWords), dictionaryKeys, valueDictionaries, avgSentences]], columns=["work", "genre", "sentences", "words", "books", "chapters", "sentences/book"])], ignore_index=True)
df = df.sort_values(by="sentences", ascending=False)
allString = " ".join(allTexts)
allSentences = nltk.sent_tokenize(allString)
allWords = nltk.word_tokenize(allString)
df = pd.concat([df, pd.DataFrame([["corpus", None, len(allSentences), len(allWords), None, None, None]], columns=["work", "genre", "sentences", "words", "books", "chapters", "sentences/book"])], ignore_index=True)
df.to_csv(f"{dataFolder}/Skripte/Data-Scraping/corpusData.csv", index=False)
with open(f"{dataFolder}/Skripte/Data-Scraping/corpusString.txt", "w") as f:
    f.write(allString)