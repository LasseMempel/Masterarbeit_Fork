from lxml import etree as ET
import os

rootDir = "/home/rudolph/repos/masterarbeit/Skripte/Data-Scraping/PerseusXML/"
parser = ET.XMLParser(resolve_entities = True, recover=True, remove_comments=True)
tags = ['bibl', 'surname', 'speaker', 'date', 'head', 'title', 'note', 'div2', 'castItem', 'l', 'body', 'persName', 'castList', 'milestone', 'sp', 'p', 'emph', 'placeName', 'gap', 'roleDesc', 'foreign', 'stage', 'div1', 'dateRange', 'role']
newTags = []
tagDir = {}

for subdir, dirs, files in os.walk(rootDir):
    for file in files:
        filePath = os.path.join(subdir, file)
        tree = ET.parse(filePath, parser)
        root = tree.getroot()
        text = root.xpath(".//text")[0]
        for element in text.iter():
            if element.tag not in tags and element.tag not in newTags:
                newTags.append(element.tag)
            if element.tag not in tagDir:
                tagDir[element.tag] = [file]
            elif file not in tagDir[element.tag]:
                tagDir[element.tag].append(file)

with open("tags.txt", "w") as f:
    f.write(str(newTags))
    f.write("\n")
    f.write(str(tagDir))
    
        