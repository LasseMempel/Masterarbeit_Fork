import json
import glob

dataFolder = "/home/rudolph/repos/masterarbeit"
files = glob.glob(f"{dataFolder}/Skripte/Data-Scraping/Iliaden/*.txt")
for file in files:
    iliadDict = {}
    author = file.split("/")[-1].strip(".txt")
    iliadDict["Book1"] = {}
    with open (file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            iliadDict["Book1"]["Line"+str(i+1)] = " ".join(lines[i].split())
    with open(f"{dataFolder}/Skripte/sourceDict/{author}_Iliad_English.json","w") as f:
        json.dump(iliadDict, f)