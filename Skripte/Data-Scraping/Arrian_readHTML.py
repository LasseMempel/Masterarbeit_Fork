from normalizeText import normalize
from bs4 import BeautifulSoup as bs
import re

def generateArrianDict():
    dataFolder = "/home/rudolph/repos/masterarbeit"
    file = f"{dataFolder}/Skripte/Data-Scraping/Anabasis of Alexander, Arrian.html"
    author = "Arrian"
    title = "Anabasis of Alexander"
    ArrianDict = {}
    with open(file, "r") as f:
        soup = bs(f.read(), "html5lib")

        # Entfernung der Seitenangaben
        span_List = [s for s in soup.find_all("span") if "pagenum" in s["class"]]
        for span in span_List:
            span.decompose()

        # Entfernung der Notes
        anchor_List = soup.find_all(class_="fnanchor")
        for anchor in anchor_List:
            anchor.decompose()

        center_List = soup.find_all(class_="center")
        for center in center_List:
            center.decompose()

        page_List = soup.find_all("a")
        for page in page_List:
            page.decompose()

        bookSoup = [b for b in soup.find_all("h2") if "book" in b.text.lower()]
        for b in bookSoup:
            book = "".join(b.text.strip(".").split(" "))
            ArrianDict[book] = {}
            text = ""
            for sibling in b.next_siblings:
                if sibling.name == "h3":
                    if text:
                        ArrianDict[book][chapter] = normalize(text)
                        text = ""
                    chapter = "".join(sibling.text.strip(".").split(" "))
                if sibling.name == "p":
                    # Rausfiltern aller Zeichen in Runden Klammern
                    text += re.sub(r"\(.*\)", "", sibling.text)
                if sibling.name == "h2":
                    ArrianDict[book][chapter] = normalize(text)
                    break
    return(ArrianDict)
