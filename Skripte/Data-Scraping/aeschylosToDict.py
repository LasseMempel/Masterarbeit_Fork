import json
import glob

dataFolder = "/home/rudolph/repos/masterarbeit"
files = glob.glob(f"{dataFolder}/Skripte/Data-Scraping/Aeschylos/*.txt")
for file in files:
    aeschylosDict = {}
    author = file.split("/")[-1].strip(".txt")
    aeschylosDict["Book1"] = {}
    with open (file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            aeschylosDict["Book1"]["Line"+str(i+1)] = " ".join(lines[i].split())
    with open(f"{dataFolder}/Skripte/sourceDict/Aeschylos/{author}_Aeschylos_English.json","w") as f:
        json.dump(aeschylosDict, f)