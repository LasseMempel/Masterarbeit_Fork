from bs4 import BeautifulSoup as bs
from natsort import natsorted
import re
import glob
from normalizeText import normalize

def generateDionysiosDict():
    
    dataFolder = "/home/rudolph/repos/masterarbeit"

    files = natsorted(glob.glob(f"{dataFolder}/Skripte/Data-Scraping/urls/*.html"))
    author = "Dionysios of Halikarnassos"
    title = "Roman Antiquities"
    DionysiosDict = {}
    for file in files:
        with open(file,"r") as f:
            soup = bs(f.read(), "html5lib")
            span_List = soup.find_all("span")
            for span in span_List:
                if span["class"][0] in ["small", "pagenum"]:
                    span.decompose()
            sec_List = soup.find_all("a", class_ = "sec") # class_ um class zu escapen
            for sec in sec_List:
                sec.decompose()
            ref_List = soup.find_all(class_ = "ref")
            for ref in ref_List:
                ref.decompose()
            bookSoup = soup.find("h1")
            bookString = bookSoup.text
            bookStringList = bookString.split()
            book = ""
            for element in bookStringList:
                if "book" in element.lower():
                    book += element.strip("(")
                    book += bookStringList[bookStringList.index(element)+1].strip(",")
                if "and" in element.lower():
                    book += f" {element} {bookStringList[bookStringList.index(element) + 1]}"
            for element in bookStringList:
                if "excerpts" in element.lower():
                    book += f" {element}"
            if book not in DionysiosDict:
                DionysiosDict[book] = {}
            pSoup = soup.find_all("p")
            for p in pSoup:
                if ("class" in p.attrs and "justify" in p["class"] and p.a and "class" in p.a.attrs and "chapter" in p.a["class"]):
                    chapterNumber = p.a.text
                    p.a.decompose()
                    # RegEx, um Klammerausdrücke mit ausschließlich Nummern und Punkten darin zu entfernen (Im Text befindliche Kapitelangaben)
                    text = re.sub(r"\([\d.]*\)", "", p.text)
                    DionysiosDict[book]["Chapter"+chapterNumber] = normalize(text)
    return DionysiosDict