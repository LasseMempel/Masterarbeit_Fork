from lxml import etree as ET
from natsort import natsorted
from normalizeText import normalize
from Arrian_readHTML import generateArrianDict
from Dionysios_readHTML import generateDionysiosDict
import glob
import json
import html
import os

def parseMetadata(file):
    with open(file, "r") as f:
        # Entfernen von kodierten "<" und ">" Zeichen, die die XML-Hierarchie stören können vor deren Dekodierung
        # &ebreve; und &obreve; werden später von html.unescape nicht umgewandelt und hier ebenfalls manuell ergänzt
        xmlString = f.read().replace("&gt;", "").replace("&lt;", "").replace("&ebreve;","ĕ").replace("&obreve;","ŏ")
        # Umwandlung verbliebener Sonderzeichen in Unicode
        # Codierung des kompletten Strings in Unicode, da er sonst den Typ bytes hat 
        xmlString = html.unescape(xmlString).encode("utf-8")
    parser = ET.XMLParser(resolve_entities = True, recover=True, remove_comments=True, encoding="utf-8")
    tree = ET.ElementTree(ET.fromstring(xmlString, parser = parser))
    root = tree.getroot()
    try:
        author = root.xpath(".//author//text()")[0]
    except:
        print(file)
        print(ET.tostring(root, pretty_print=True)[0:1000])
        print(title = root.xpath(".//titleStmt//title//text()")[0].split("(")[0].rstrip())
        raise ValueError("No author found!")
    title = root.xpath(".//titleStmt//title//text()")[0].split("(")[0].rstrip()
    if author == "Plutarch":
        title = root.xpath(".//titleStmt//title//text()")[1].split("(")[0].rstrip()
    #language = " ".join(root.xpath(".//profileDesc//langUsage//language")[0].text.split())
    return root, author, title

def stripRoot(element, title):
    bodies = element.xpath(".//body")
    if title == "The Foreign Wars": 
        print("Dealing with Appians bodies...")
        for body in bodies[:3]: # Entfernen der 3 einführenden bodies von Appian foreign wars
            body.getparent().remove(body)
    elif len(bodies) > 1:
        if title not in ["Antiquities of the Jews"]: # Antiquities of the Jews besitzt einen weiteren body innerhalb des root-body
            raise ValueError(f"Too many bodies! {title} has {len(bodies)} bodies...")
    return element
    
def createSourceDic(root, author, title, stripList):
    def sectionStrings(item, seperatorList, defaultType):
        if item.get("type") != None:
            itemType = item.get("type")
        else:
            itemType = defaultType
        if item.get("n") != None:
            itemNumber = str(item.get("n"))
        else:
            # Wenn item keine Nummer hat, ist dessen Nummer der Index der Liste der Elemente die den gleichen Type des items haben +1
            itemNumber = str([x for x in seperatorList if x.get("type") == item.get("type")].index(item) + 1)
        # Wenn es mehrere Elemente mit dem gleichen Type und der gleichen Nummer gibt, wird die Nummer um _ + den Index des Elements in der Liste +1 ergänzt
        if len([x for x in seperatorList if (x.get("type") == item.get("type") and x.get("n") == item.get("n") and x.get("n") != None)]) > 1:
            itemNumber += "_"+str([x for x in seperatorList if (x.get("type") == item.get("type") and x.get("n") == item.get("n") and x.get("n") != None)].index(item) + 1)
            #print("Doublette!")
            #print(itemType+itemNumber)
        return itemType+itemNumber

    # Rekursives Auslesen von Text und Text-Tail ausgewählter Elemente
    def textRec(start, element, elementStringList, stripList):
        if element.tag not in stripList:
            if element.text:
                elementStringList.append(element.text)
            for child in element:
                elementStringList = textRec(start, child, elementStringList, stripList)
            if element.tail and element.tag != start.tag:
                elementStringList.append(element.tail)
        else:
            if element.tail and element.tag != start.tag:
                elementStringList.append(element.tail)    
        return elementStringList
    
    # check if text in section parent before first seperator element
    def checkPreText(element, sectionParent):
        pretext = ""       
        # is element first occurence of tag in parent, not body and not child of same tag? (latter: fix for xenophon cyropaedia)
        if (element.tag != "body" and element == (sectionParent.findall(element.tag))[0] and sectionParent.tag != element.tag): 
            for child in sectionParent.getchildren():
                # Abbruch der Schleife bei Erreichen des 1. Siblings
                if child.tag == element.tag:
                    break
                else:
                    pretext += normalize(" ".join(textRec(child, child, [], stripList)))
        return pretext

    fileDict = {}
    bodies = root.xpath(".//body")      
    seperator1 = "div1"
    seperator2 = "div2"
    parent = bodies[0]
    # Appians foreign wars enthält die Bücher nicht in div1-, sondern in body-containern
    if title == "The Foreign Wars":
        seperator1 = "body"
        seperator2 = "div1"
        parent = root
    # Homers Epen enthalten generische Div Tags für Bücher und Kapitel sowie ein weiteres umfassendes Div Tag
    if author == "Homer":
        seperator1 = "div[@subtype='book']" # Da sonst alle Div-Container gefunden werden, wird hier das Attribut subtype konkretisiert
        seperator2 = "div[@subtype='card']"
    if author == "Plutarch":
        seperator1 = "body"
        parent = parent.getparent() # Body ist Kind von <text>, was sich bei einer xpath-Suche nicht findet

    seperator1List = parent.xpath(f".//{seperator1}")
    for book in seperator1List:
        bookString = sectionStrings(book, seperator1List, "Book")
        if not bookString in fileDict:
            fileDict[bookString] = {}
        else:
            raise ValueError("book duplicate in dict")
        seperator2List = book.xpath(f".//{seperator2}")
        if len(seperator2List) > 0:
            for chapter in seperator2List:
                chapterString = sectionStrings(chapter, seperator2List, "Chapter")
                if chapterString in fileDict[bookString]:
                    print(f"duplicate {chapterString} in {bookString}.")
                    raise ValueError("chapter duplicate in dict")
                preText = checkPreText(chapter, chapter.getparent())
                if preText:
                    fileDict[bookString][chapterString+"_pretext"] = preText
                    preTextOccurences.append((author, title, bookString, chapterString))
                text = normalize(" ".join(textRec(chapter, chapter, [], stripList)))
                if text: # " ".join[] will result in "" and creates a useless key in the dict
                    fileDict[bookString][chapterString] = text
        else:
            preText = checkPreText(book, book.getparent())
            if preText:
                fileDict[bookString+"_pretext"] = preText
                preTextOccurences.append((author, title, bookString))
            text = normalize(" ".join(textRec(book, book, [], stripList)))
            if text:
                fileDict[bookString] = text
    print(len(fileDict))
    return fileDict

def main():
    global dataFolder
    dataFolder = "/home/rudolph/repos/masterarbeit"
    global preTextOccurences
    preTextOccurences = []
    rootDir = f"{dataFolder}/Skripte/Data-Scraping/PerseusXML/"
    for subdir, dirs, files in os.walk(rootDir):
        for file in files:
            genre = subdir.split("/")[-1]
            root, author, title = parseMetadata(os.path.join(subdir, file))
            print(f"Adding {author} {title}")
            root = stripRoot(root, title)
            stripList = ["argument" ,"bibl", "castItem", "castList", "cell", "figDesc", "figure", "head", "note", "stage", "speaker"]
            if not os.path.exists(projectedFolder):
                os.makedirs(projectedFolder)
            with open(f"{projectedFolder}/{author}_{title}.json","w") as f:
                workDict = createSourceDic(root, author, title, stripList)
                # Book17 consists just of a single note
                if author == "Polybius":
                    #delete book17 from workDict
                    print("Deleting book17 from Polybius")
                    del workDict["book17"]
                # chapter1_pretext is just a note
                if title == "The Civil Wars":
                    print("Deleting chapter1_pretext of bookfragments from Appian Civil Wars")
                    del workDict["bookfragments"]["chapter1_pretext"]
                json.dump(workDict, f)         
main()

print("Adding Arrians Anabasis")
ArrianDict = generateArrianDict()
with open(f"{dataFolder}/Skripte/sourceDict/Geschichtswerke/Arrian_Anabasis of Alexander.json","w") as f:
    json.dump(ArrianDict, f)

print("Adding Dionysios Roman Antiquities")
DionysiosDict = generateDionysiosDict()
with open(f"{dataFolder}/Skripte/sourceDict/Geschichtswerke/Dionysios of Halikarnassos_Roman Antiquities.json","w") as f:
    json.dump(DionysiosDict, f)

# load all json files from Demosthenes in sourceDict and merge their keys into one dictionary
print("Merging Demosthenes Speeches")
DemosthenesDict = {}
for file in natsorted(glob.glob(f"{dataFolder}/Skripte/sourceDict/Rhetorik/Demosthenes*.json")):
    with open(file, "r") as f:
        DemosthenesDict.update(json.load(f))
    # delete file
    os.remove(file)
with open(f"{dataFolder}/Skripte/sourceDict/Rhetorik/Demosthenes_Speeches.json","w") as f:
    json.dump(DemosthenesDict, f)


# load all json files from Plutarch in sourceDict and merge their keys into one dictionary
print("Merging Plutarch Lives")
PlutarchDict = {}
for file in natsorted(glob.glob(f"{dataFolder}/Skripte/sourceDict/Biographie/Plutarch*.json")):
    with open(file, "r") as f:
        biographyDict = json.load(f)
        biographyName = file.split("_")[1].split(".json")[0]
        PlutarchDict[biographyName] = biographyDict["Book1"]
    os.remove(file)
with open(f"{dataFolder}/Skripte/sourceDict/Biographie/Plutarch_Lives.json","w") as f:
    json.dump(PlutarchDict, f)

# load all json files from Appian in sourceDict and merge their keys into one dictionary
print("Merging Appian Roman History")
AppianDict = {}
# natsorted in reverse order, so earlier books are added first
for file in natsorted(glob.glob(dataFolder+"/Skripte/sourceDict/Geschichtswerke/Appian*.json"), reverse=True):
    with open(file, "r") as f:
        if file == f"{dataFolder}/Skripte/sourceDict/Geschichtswerke/Appian_The Civil Wars.json":
            bookNumbers = ["Civil Wars 13", "Civil Wars 14", "Civil Wars 15", "Civil Wars 16", "Civil Wars 17", "BookFragments"]
            CivilWarsDict = json.load(f)
            for bookNumber, key in zip(bookNumbers, CivilWarsDict.keys()):
                AppianDict[bookNumber] = CivilWarsDict[key]
        elif file == f"{dataFolder}/Skripte/sourceDict/Geschichtswerke/Appian_The Foreign Wars.json":
            bookNumbers = ["Kings"," Italy", "Samnite History", "Gallic History", "Sicily and the Other Islands", "Wars in Spain", \
            "Hannibalic War", "Punic Wars", "Numidian Affairs", "Macedonian Affairs", "Illyrian Wars", "Syrian Wars", "Mithridatic Wars"]
            ForeignWarsDict = json.load(f)
            for bookNumber, key in zip(bookNumbers, ForeignWarsDict.keys()):
                AppianDict[bookNumber] = ForeignWarsDict[key]
    os.remove(file)
with open(f"{dataFolder}/Skripte/sourceDict/Geschichtswerke/Appian_Roman History.json","w") as f:
    json.dump(AppianDict, f)

