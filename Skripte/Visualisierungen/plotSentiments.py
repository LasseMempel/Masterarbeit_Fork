from matplotlib import pyplot as plt
import pandas as pd
import os
import json

dataFolder = "/home/rudolph/repos/masterarbeit"

models = ["siebert-sentiment-roberta-large-english", "roberta-large-mnli2Classes", "facebook-bart-large-mnli2Classes","ensemble"] 
genres = ["Biographie", "Epik", "Geographie", "Geschichtswerke", "Komödie", "Philosophie", "Rhetorik", "Tragödien"]

def buildLinePlots(dataFolder, models, genres):
    with open(dataFolder + "/Skripte/Analyse/sentData/sentData.json", "r") as f:
        sentDict = json.load(f)
        for model in models:
            for key, value in sentDict.items():
                title, author = key.split("_")
                genre = value["genre"]
                projectedPath = dataFolder + "/Skripte/Visualisierungen/lineplots/" + model + "/" + genre + "/"
                if not os.path.exists(projectedPath):
                    os.makedirs(projectedPath)
                plt.figure(figsize=(20,20))
                plt.plot(value[model]["cumulatedSentList"], label=author + " " + title)
                plt.tight_layout()
                plt.savefig(projectedPath + author + "_" + title + ".png")
                plt.clf()
                plt.close()
                if genre not in ["Tragödien", "Komödie"]:
                    if value[model]["bookSentList"]:
                        for bookList, bookName in  zip(value[model]["cumulatedBookSentList"], value["bookStrings"]):
                            plt.plot(bookList, label=author + " " + title + " " + bookName)
                            bookPath = projectedPath + "/" + author + "_" + title + "/books/"
                            if not os.path.exists(bookPath):
                                os.makedirs(bookPath)
                            plt.savefig(bookPath + bookName + ".png")
                            plt.clf()
                            plt.close()
        for genre in genres:
            for model in models:
                plt.figure(figsize=(20,20))
                plt.xlabel("Satznummer", fontsize="20")
                plt.ylabel("Sentiment", fontsize="20")
                for key, value in sentDict.items():
                    title, author = key.split("_")
                    if value["genre"] == genre:
                        plt.plot(value[model]["cumulatedSentList"], label=author + " " + title)
                plt.legend(fontsize="20")
                plt.xticks(fontsize="20")
                plt.yticks(fontsize="20")
                plt.tight_layout()
                plt.savefig(dataFolder + "/Skripte/Visualisierungen/lineplots/" + model + "/" + genre + ".png")
                plt.clf()
                plt.close()
            if genre == "Tragödien":
                for model in models:
                    for tragician in ["Euripides", "Aeschylus", "Sophocles"]:
                        plt.figure(figsize=(20,20))
                        plt.xlabel("Satznummer", fontsize="20")
                        plt.ylabel("Sentiment", fontsize="20")
                        for key, value in sentDict.items():
                            title, author = key.split("_")
                            if value["genre"] == genre and author == tragician:
                                plt.plot(value[model]["cumulatedSentList"], label=author + " " + title)
                        plt.legend(fontsize="20")
                        plt.xticks(fontsize="20")
                        plt.yticks(fontsize="20")
                        plt.tight_layout()
                        plt.savefig(dataFolder + "/Skripte/Visualisierungen/lineplots/" + model + "/" + genre + "/" + tragician + ".png")
                        plt.clf()
                        plt.close()
#buildLinePlots(dataFolder, models, genres)

def buildBarPlots():
    for model in models:
        with open(dataFolder + f"/Skripte/Analyse/sentData/{model}/labelValuesWorks.csv") as f:
            df = pd.read_csv(f)
        df = df[df["genre"] == "Geschichtswerke"]
        df = df.sort_values(by=["sentiment"])
        plt.figure(figsize=(20,20))
        plt.bar(df["title"], df["sentiment"])
        plt.xticks(rotation=90)
        plt.xticks(fontsize="20")
        plt.yticks(fontsize="20")
        plt.tight_layout()
        plt.savefig(dataFolder + f"/Skripte/Visualisierungen/barplots/{model}.png")
        plt.clf()
        plt.close()
    df = df.sort_values(by=["emotionality"])
    plt.figure(figsize=(20,20))
    df["sentiment"] = df["sentiment"] * -0.5
    df["emotionality"] = df["emotionality"] * 0.25
    df["suffering"] = df["suffering"] * 0.25
    plt.bar(df["title"], df["emotionality"], label="Emotionalität")
    plt.bar(df["title"], df["suffering"], bottom=df["emotionality"], label="Leid")
    plt.bar(df["title"], df["sentiment"], bottom=df["emotionality"] + df["suffering"], label="Sentiment")
    plt.xticks(rotation=90)
    plt.xticks(fontsize="20")
    plt.yticks(fontsize="20")
    plt.legend(fontsize="20", loc = "upper left", title = "Gewichtete Merkmale", title_fontsize="20")
    plt.ylabel("'Tragik'", fontsize="20")
    plt.tight_layout()
    plt.savefig(dataFolder + "/Skripte/Visualisierungen/barplots/GeschichtswerkeStackedTragicness.png")
    plt.clf()
    plt.close()
buildBarPlots()

def buildStackPlot():
    with open(dataFolder + "/Skripte/Analyse/sentData/ensemble/labelValuesWorks.csv") as f:
        df = pd.read_csv(f)
    df = df[df["genre"] == "Geschichtswerke"]
    plt.figure(figsize=(20,20))
    plt.ylabel("'Tragik'")
    df["sentiment"] = df["sentiment"] * -0.5
    df["emotionality"] = df["emotionality"] * 0.25
    df["suffering"] = df["suffering"] * 0.25
    plt.stackplot(df["title"], df["emotionality"], df["suffering"], df["sentiment"], labels=["Emotionalität", "Leid", "Sentiment"])
    plt.xticks(rotation=90)
    plt.xticks(fontsize="20")
    plt.yticks(fontsize="20")
    plt.legend(fontsize="20", loc = "upper left")
    plt.tight_layout()
    plt.savefig(dataFolder + "/Skripte/Visualisierungen/stackplots/GeschichtswerkeStackedTragicness.png")
    plt.clf()
    plt.close()
#buildStackPlot()

def buildPieCharts():
    with open(dataFolder + "/Skripte/Analyse/sentData/ensemble/labelValuesWorksPure.csv") as f:
        df = pd.read_csv(f)
    emotions = ["anger","disgust","fear","joy","sadness","surprise","neutral"]
    for genre in genres:
        plt.figure(figsize=(20,20))
        plt.title(genre)
        plt.pie(df[df["genre"] == genre][emotions].sum(), labels=emotions, autopct='%1.1f%%')
        plt.savefig(dataFolder + "/Skripte/Visualisierungen/piecharts/" + genre + ".png")
        plt.clf()
        plt.close()
    for key, value in df.iterrows():
        plt.figure(figsize=(20,20))
        plt.title(value["title"])
        plt.pie(value[emotions], labels=emotions,)
        if not os.path.exists(dataFolder + "/Skripte/Visualisierungen/piecharts/" + value["genre"]):
            os.makedirs(dataFolder + "/Skripte/Visualisierungen/piecharts/" + value["genre"])
        plt.savefig(dataFolder + "/Skripte/Visualisierungen/piecharts/" + value["genre"] + "/" + value["title"] + ".png")
        plt.clf()
        plt.close()
    plt.figure(figsize=(20,20))
    fig, axs = plt.subplots(2, 2)
    axs[0, 0].pie(df[df["genre"] == "Epik"][emotions].sum())
    axs[0, 0].set_title("Epik")
    axs[0, 1].pie(df[df["genre"] == "Tragödien"][emotions].sum())
    axs[0, 1].set_title("Tragödien")
    axs[1, 0].pie(df[df["genre"] == "Geschichtswerke"][emotions].sum())
    axs[1, 0].set_title("Geschichtswerke")
    axs[1, 1].pie(df[df["genre"] == "Geographie"][emotions].sum())
    axs[1, 1].set_title("Geographie")
    fig.legend(labels=emotions, fontsize="10")
    plt.savefig(dataFolder + "/Skripte/Visualisierungen/piecharts/genres.png")
    plt.clf()
    plt.close()
#buildPieCharts()

def buildBoxPlots():
    with open(dataFolder + "/Skripte/Analyse/sentData/ensemble/chapterTragicness.csv") as f:
        df = pd.read_csv(f)
    authors = df.author.unique()
    plt.figure(figsize=(20,20))
    plt.xlabel("Autor")
    plt.ylabel('Tragik')
    plt.boxplot([df[df["author"] == author]["tragicness"] for author in authors], labels=authors)
    plt.savefig(dataFolder + "/Skripte/Visualisierungen/boxplots/chapterTragicness.png")
    plt.clf()
    plt.close()
    with open(dataFolder + "/Skripte/Analyse/sentData/ensemble/tragedyBookTragicness.csv") as f:
        df = pd.read_csv(f)
    plt.figure(figsize=(20,20))
    df = df[df["genre"] == "Tragödienbuch"]
    plt.ylabel("'Tragik'", fontsize="20")
    plt.boxplot([df["tragicness"]])
    plt.xticks([])
    plt.yticks(fontsize="20")
    plt.savefig(dataFolder + "/Skripte/Visualisierungen/boxplots/tragedyBookTragicness.png")
    plt.clf()
    plt.close()
#buildBoxPlots()














